#ifndef FRAMES_H
#define FRAMES_H
// ^ Header guard

// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include <vector>


class container; // Forward declaration of the container class

// Abstract base class for the ASCII art frames
class frame
{
public:
	virtual ~frame() = default; // Virtual destructor

	virtual void dispFrame() = 0; // Pure virtual function to display the frame
};


// Derived class for the main investment frame
class invest : public frame
{
private:
	std::pair<int, int> currentSel; // Tracking where the user's selection is on the investment menu: .first is the mode of transport and .second is the container type (default to `0,0')
	std::vector<std::string> transportNames, containerNames; // Reference lists containing the name of the transport modes and container types
	std::vector<int> maxLoads, costs; // Reference vectors for the maximum number of containers that each transport can hold, and how much each container type costs
	bool departed; // Flag for if the loaded containers have departed
	bool toMenu; // Flag for if the user wants to return from the investment screen to the menu
	bool fileAgree; // Flag to see if the user has agreed to the creation of a file
	bool neverStarted; // Flag to see if the user has actually ever been to the invest screen before

public:
	invest(); // Default constructor
	~invest(); // Destructor

	// Static variables, with scope across multiple class instances
	static std::vector<container*> cargoList; // Base class pointer vector to store all of the chosen cargo
	static std::vector<int> numLoaded; // Vector for how many of each container type is loaded
	static int money; // The user's money, which is deducted for loading cargo and increased by finishing a transport run and delivering cargo
	static bool gameProg; // Flag for if the user has loaded cargo, but hasn't yet departed

	static bool getProg()
	{ // Static function to return the game in progress flag
		return gameProg;
	}

	static void resetProg(); // Reset the game in progress flag
	static void resetCargoList(); // Reset the cargo list
	static void resetNumLoaded(); // Reset the number of cargo loaded
	static void resetMoney(); // Reset the user's money

	// Gets
	bool getDeparted() const
	{ // Return flag to see if the user has chosen to depart with their loaded cargo
		return departed;
	}

	bool getToMenu() const
	{ // Return flag to see if the user wants to return back to the main menu
		return toMenu;
	}

	bool getFileAgree() const
	{ // Return flag to see if the user has agreed to the creation of the save file
		return fileAgree;
	}
	
	bool getNeverStarted() const
	{ // Return flag to see if the user has ever started a single game
		return neverStarted;
	}


	// Main game functions
	void dispFrame() override; // Display the investment screen where the user can select their cargo
	void addCargo(); // Display the `add selected cargo' screen

	void loopSelection(const int incrementMode, const int incrementType); // Loop the user's selection on the `select cargo' screen depending on their inputs

	void saveFile(); // Save current game into a file
	void loadFile() const; // Load previous game file
};


// Derived class for ship-based containers in water
class frameSea : public frame
{
public:
	frameSea() = default; // Default constructor
	~frameSea() = default; // Destructor

	void dispFrame() override; // Overriden function to display the ship frame
};


// Derived class for truck-based containers on land
class frameLand : public frame
{
public:
	frameLand() = default; // Default constructor
		~frameLand() = default; // Destructor

	void dispFrame() override; // Overriden function to display the ship frame
};


// Derived class for plane-based containers in air
class frameAir : public frame
{
public:
	frameAir() = default; // Default constructor
	~frameAir() = default; // Destructor

	void dispFrame() override; // Overriden function to display the ship frame
};


// Derived class for displaying the results of the transported cargo
class receipt : public frame
{
	template<typename T> // Friend template to take the difference from two vectors
	friend std::vector<T> operator-(const std::vector<T>& vecA, const std::vector<T>& vecB);

private:
	int currentSel; // Tracking which cargo is currently selected on the list
	void loopCargo(const int increment); // Loop the cargo selection, depending on the user's input

public:
	receipt() : currentSel(0) {} // Default constructor, start currentSel at the top of the list
	~receipt() = default; // Destructor

	void dispFrame() override; // Overriden function to display the receipt frame
};

// Template friend function to take the difference between two vectors
template <typename T>
std::vector<T> operator-(const std::vector<T>& vecA, const std::vector<T>& vecB)
{
	std::vector<T> vecDiff; // Create the new vector
	for (auto iter = vecA.begin(); iter < vecA.end(); ++iter)
	{ // Iterate through the points
		vecDiff.push_back(*iter - vecB.at(iter - vecA.begin())); // Take the difference
	}

	return vecDiff; // Return the differenced vector
}


// End of header guard
#endif // FRAMES_H
