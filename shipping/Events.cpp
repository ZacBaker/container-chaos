// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include "Events.h"

#include "Frames.h"
#include "Containers.h"


// Default constructor for the random events
event::event() : exampleEventPositive{false}, exampleFound{false}
{
	// Initialise the 2D vector for good (.at(0)) and bad (.at(1)) event modifing values
	eventModifier = { std::vector<double>(0), std::vector<double>(0) };

	// Only generate events for the desired transport medium
	switch (invest::cargoList.at(0)->getMedium())
	{
		case 0: // Initialise the ship-based messages (using a C++11 sepcific method!)
			{
				// Positive messages and modifier values
				eventListGood = { "The wind was in your favour",
					"Poseidon smiles kindly upon you this day",
					"Nothing but smooth sailing",
					"Clear skies today",
					"The Bermuda Triangle has warped you closer to port!",
					"Market prices have gone up",
					"Someone has invested in your company",
					"Took a shortcut through a canal",
					"Special fuel is making your engine better",
					"The coast guard is clearing you a path",
					"Everyone is working super efficiently",
					"A tsunami has boosted you!" };
				eventModifier.at(0) = { 0.1, 0.2, 0.1, 0.1, 0.25, 0.15, 0.2, 0.1, 0.2, 0.2, 0.15, 0.3 };
				// Negative messages and modifier values
				eventListBad = { "There was a crack in one the containers",
					"A container has leaked its contents",
					"A container has been damaged",
					"Pirates have attacked the ship!",
					"Poseidon's wrath has caught up to you!",
					"Tidal wave!!",
					"Engine failure, time to row!",
					"Hugged an iceburg",
					"Penguins have invaded the ship!",
					"Those robots from Pacific Rim are having a fight",
					"The Bermuda Triangle has led you in 3-sided circles!",
					"Had to stop to respond to a mayday call" };
				eventModifier.at(1) = { -0.1, -0.1, -0.1, -0.2, -0.25, -0.2, -0.15, -0.1, -0.2, -0.1, -0.2, -0.1 };
		}
			break;
		case 1: // Initialise the truck-based messages (using a C++11 sepcific method!)
		{
			// Positive messages and modifier values
			eventListGood = { "The wind was in your favour",
				"Someone has invested in your company",
				"Nothing but green lights",
				"Clear skies today",
				"Everyone else has stayed at home today",
				"Market prices have gone up",
				"Empty roads",
				"Took a shortcut",
				"Special fuel is making your engine better",
				"Witness me!!!",
				"Police escorts are freeing up your route",
				"The unloading team is ready for you" };
				eventModifier.at(0) = { 0.1, 0.15, 0.1, 0.1, 0.25, 0.15, 0.2, 0.1, 0.2, 0.15, 0.1, 0.3 };
				// Negative messages and modifier values
				eventListBad = { "There was a crack in one the containers",
					"A container has leaked its contents",
					"A container has been damaged",
					"Bikers have attacked the truck!",
					"The police have pulled you over",
					"Earthquake!!",
					"Engine failure, time to push!",
					"Scraped a metal post",
					"Kangaroos have attacked the truck!",
					"Time for a regulation nap",
					"A tire has blown out",
					"Had to stop to help someone cross the road" };
				eventModifier.at(1) = { -0.1, -0.1, -0.1, -0.2, -0.1, -0.2, -0.2, -0.1, -0.3, -0.15, -0.2, -0.1 };
		}
			break;
		case 2: //Initialise the plane-based messages (using a C++11 sepcific method!)
			{
				// Positive messages and modifier values
				eventListGood = { "The wind was in your favour",
					"A jet is pushing you along!",
					"Nothing but smooth flying",
					"Clear skies today",
					"The jet stream has reduced your journey time",
					"Market prices have gone up",
					"Someone has invested in your company",
					"Empty skies",
					"Special fuel is making your engine better",
					"Everyone is working super efficiently",
					"The unloading team is ready for you",
					"A hurricane boosted you!" };
				eventModifier.at(0) = { 0.1, 0.2, 0.2, 0.1, 0.15, 0.15, 0.1, 0.1, 0.2, 0.4, 0.2, 0.1 };
				// Negative messages and modifier values
				eventListBad = { "There was a crack in one the containers",
					"A container has leaked its contents",
					"A container has been damaged",
					"There are snakes loose on the plane!",
					"The Sun is in your eyes",
					"Hurricane!!",
					"Engine failure, time to glide!",
					"Lost in the clouds",
					"Eagles have attacked the plane!",
					"Aliens abducted you for a bit",
					"Fire on the plane!",
					"Had to divert for a medical emergency" };
				eventModifier.at(1) = { -0.1, -0.1, -0.1, -0.2, -0.25, -0.2, -0.15, -0.1, -0.2, -0.1, -0.15, -0.1 };
		}
			break;
	}
}

// Member function to generate the events
void event::genEvent()
{
	exampleFound = false; // Reset the example flag

	// Generate a two random distributions, based on the Mersenne Twister 2^(19937)-1. See: http://www.cplusplus.com/reference/random/
	std::random_device seed;
	std::mt19937 mtGen(seed());
	std::uniform_int_distribution<int> percDist(0, 100); // A distribution for a random percentage
	std::uniform_int_distribution<int> eventDist(0, 11); // A distribution for a random event (there are 12 events in all transport modes and positive/negative)

	// Iterate through each of the loaded cargo containers
	for (auto iter = invest::cargoList.begin(); iter < invest::cargoList.end(); ++iter)
	{
		// Test whether to have an event or not
		if (percDist(mtGen) > 60)
		{ // 40% chance for an event to happen

			// Test if the event should be good or bad
			if (percDist(mtGen) > 60)
			{ // Bad event (40% of the time)

				int choice = eventDist(mtGen); // Need to initialise the choice, as we want the same index in both the eventList and eventModifier

				// Modify the value of the current container
				(*iter)->setValue(eventModifier.at(1).at(choice));

				if (!exampleFound) // Check if the example has not yet been chosen
				{ // Only want 1 example to display to the user per time
					exampleEvent = eventListBad.at(choice); // Set the example message
					exampleEventPositive = false; // Set the flag for the message's polarity
					// Set the flag so this doesn't trigger again for the other containers
					exampleFound = true;
				}
			}
			else
			{ // Good event (60% of the time)

				std::uniform_int<>::result_type choice = eventDist(mtGen); // Need to initialise, as we want the same index in both the eventList and eventModifier

				// Modify the value of the current container
				(*iter)->setValue(eventModifier.at(0).at(choice));

				if (!exampleFound) // Check if the example has not yet been chosen
				{ // Only want 1 example to display to the user per time
					exampleEvent = eventListGood.at(choice); // Set the example message
					exampleEventPositive = true; // Set the flag for the message's polarity
					// Set the flag so this doesn't trigger again for the other containers
					exampleFound = true;
				}
			}
		}
	}
}
