#ifndef EVENTS_H
#define EVENTS_H
// ^ Header guard

// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include <random> // C++11 only


// Event class, for random events occuring during a trip
class event
{
private:
	// Private functions
	std::vector<std::string> eventListGood, eventListBad; // Good and bad event strings
	std::vector<std::vector<double>> eventModifier; // The relative weightings for all events: .at(0) is good events, .at(1) is bad events
	std::string exampleEvent; // An example event string
	bool exampleEventPositive; // Whether the example event was positive or negative

	// Private variables
	bool exampleFound;

public:
	event(); // Default constructor
	~event() {}; // Destructor

	void genEvent(); // Public function to generate an event
	std::pair<bool, std::string> getExampleEvent() const // Returns the example event string
	{
		return make_pair(exampleEventPositive, exampleEvent);
	}
};

// End of header guard
#endif // EVENTS_H
