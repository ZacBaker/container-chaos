// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include "Support.h"


// Overloaded output stream operator<< to set the colour
// Reference: https://msdn.microsoft.com/en-us/library/windows/desktop/ms686047(v=vs.85).aspx
std::basic_ostream<char>& operator<<(std::basic_ostream<char>& ostream, const setCol& colStruct)
{
	// Calculate the code that needs to be passed to the modifying system function below
	int code = static_cast<int>(colStruct.colTxt) + 16 * static_cast<int>(colStruct.colBG);

	// Modify the text colour and background colour
	SetConsoleTextAttribute(colStruct.consoleHandle, static_cast<WORD>(code));

	return ostream; // Return the modified output stream
}


// Stand-alone system function to capture and return the key inputs from the console
INPUT_RECORD getKeyPress()
{ // Reference: https://msdn.microsoft.com/en-us/library/windows/desktop/ms684961(v=vs.85).aspx

	HANDLE inputHandle = GetStdHandle(STD_INPUT_HANDLE); // Handle to the console, see: https://msdn.microsoft.com/en-gb/library/windows/desktop/ms683231(v=vs.85).aspx
	DWORD numEvents = 0; // The number of events
	DWORD numEventsRead = 0; // The number of events read from the console
	INPUT_RECORD inputRecord;

	// Extract the number of console inputs
	GetNumberOfConsoleInputEvents(inputHandle, &numEvents);

	// Read data from the console input buffer and removes it from said buffer
	ReadConsoleInput(inputHandle, &inputRecord, 1, &numEventsRead);

	// Key codes for possible output values: https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx
	return inputRecord; // Return the input record to compare against the possible options
}
