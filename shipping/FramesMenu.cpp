// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include<memory> // For std::unique_ptr<class>

#include "FramesMenu.h"
#include "Containers.h"


void title::mainMenu()
{
	// Define and initialise the instances of our game
	std::unique_ptr<invest> game(new invest());

	// Run the main menu loop until the user chooses to exit the program
	bool menuLoop{ true };
	while (menuLoop)
	{
		// Define and initialise some logic variables for use in the menu
		bool play{ false }; // Go to the investment screen? (false = no, true = yes)
		bool load{ false }; // Load a previous game from file? (false = no, true = yes)
		bool continueGame{ false }; // Continue a game currently in session? (false = no, true = yes)

		// Run the main menu selection loop until the user has chosen a valid menu input
		bool chooseLoop{ true };
		while (chooseLoop)
		{
			// Display the main menu's frame
			this->dispFrame();

			bool invalidKey{ true };
			while (invalidKey)
			{ // Loop until we get a valid key input, otherwise the screen will refresh on every input!
				// Monitor and capture the user's keyboard inputs
				INPUT_RECORD choice = getKeyPress(); // Grab the input
				if (choice.Event.KeyEvent.bKeyDown)
				{ // Only enter if a key has been pressed

					// Key press was: 5 or Numpad 5
					if (choice.Event.KeyEvent.wVirtualKeyCode == 0x35 || choice.Event.KeyEvent.wVirtualKeyCode == VK_NUMPAD5)
					{ // User has chosen to exit the program
						invalidKey = false; // Break the key validation loop

						if (invest::cargoList.empty() && !game->getFileAgree() && !game->getNeverStarted()) // User had to pass a save check after exiting the receipt, so only prompt to save if they declined to do it. Don't prompt if the game has yet to be started (i.e. program run, then exit before doing a single investment)
						{ // Only prompt the user to save progress if no cargo has been loaded, i.e. the user has not spent any money this round!

							bool msgDisplayed{ false }; // Create a bool to test if the warning message has been displayed - don't want the message to be shown again on every key press!

							// Loop until the user has chosen a valid input
							bool saveLoop{ true };
							while (saveLoop)
							{
								if (!msgDisplayed)
								{ // Only display the message if it isn't already on screen
									// Display the warning message
									std::cout << std::endl << setCol(RED) << "  Would you like to save your game before quitting? " << setCol(WHITE, RED) << " Y " << setCol(RED) << " or " << setCol(WHITE, RED) << " N " << setCol(RED) << ": " << setCol(WHITE);
									msgDisplayed = true; // Message has been shown, record this
								}
								INPUT_RECORD irInput = getKeyPress(); // Get their input
								if (!irInput.Event.KeyEvent.bKeyDown)
								{
									switch (irInput.Event.KeyEvent.wVirtualKeyCode)
									{
										// Key press was: Y
									case 0x59: // User has agreed to save their game first
										std::cout << std::endl; // Space to make sure the green 'saving...' text doesn't overflow the line
										try
										{ // Try to save the progress
											game->saveFile();
										}
										catch (std::runtime_error err)
										{ // Error, file could not be saved
											// Display the error message
											std::cout << std::endl << setCol(RED) << err.what() << setCol(WHITE) << std::endl;
										}
										saveLoop = false; // Valid input, so exit the loop
										break;
										// Key press was: N
									case 0x4e: // User does not want to save their game first
										saveLoop = false; // Valid input, so exit the loop
										break;
									}
								}
							}
						}
						// Exit all loops so the program can end
						menuLoop = false;
						chooseLoop = false;
					}
					// Key press was: 4 or Numpad 4
					if (choice.Event.KeyEvent.wVirtualKeyCode == 0x34 || choice.Event.KeyEvent.wVirtualKeyCode == VK_NUMPAD4)
					{ // User has chosen to view the container reference page
						invalidKey = false; // Break the key validation loop

						reference referenceBase = reference(); // Create a new instance of the reference page class
						reference referencePage(referenceBase); // Create a new instance using the copy constructor (definitely not a required step, just a proof of concept for the copy constructor!)

						while (true) // I acknowledge the warning "warning C4127: conditional expression is constant", but short of suppressing this warning, there is nothing wrong with using while(true)!
						{ // Need to run a loop for the swapping/moving of the classes,
							referencePage.resetSwapMode(); // Reset the swap flag so the logic will work
							referencePage.dispFrame(); // Display the frame

							// Check if the user chose to swap from within the reference page
							if (referencePage.getSwapMode())
							{ // This swap will keep all of the same values/positions from the reference page the user was just in
								reference referencePageSwap(std::move(referencePage)); // Create a new instance using the move constructor
								referencePageSwap.dispFrame(); // Display the swapped frame

								// Swap the instances back using the move assignment operator
								referencePage = std::move(referencePageSwap);
							}
							else
							{ // User chose to return to the main menu and not swap, so break from the loop
								break;
							}
						}
					}
					// Key press was: 3 or Numpad 3
					if (choice.Event.KeyEvent.wVirtualKeyCode == 0x33 || choice.Event.KeyEvent.wVirtualKeyCode == VK_NUMPAD3)
					{ // User has chosen to view the about page
						invalidKey = false; // Break the key validation loop
						std::unique_ptr<about> aboutPage(new about()); // Create a new instance of the about page class
						aboutPage->dispFrame(); // Display the frame
						// `about' instance will be destroyed when `aboutPage' goes out of scope
					}
					// Key press was: 2 or Numpad 2
					if (choice.Event.KeyEvent.wVirtualKeyCode == 0x32 || choice.Event.KeyEvent.wVirtualKeyCode == VK_NUMPAD2)
					{ // User has chosen to continue from a previous game
						invalidKey = false; // Break the key validation loop
						load = true; // Set flag to load
						play = true; // Set flag to play
						chooseLoop = false; // Valid input, exit the choose loop
					}
					// Key press was: 1 or Numpad 1
					if (choice.Event.KeyEvent.wVirtualKeyCode == 0x31 || choice.Event.KeyEvent.wVirtualKeyCode == VK_NUMPAD1)
					{ // User has chosen to start a new game
						invalidKey = false; // Break the key validation loop
						play = true; // Set flag to play
						chooseLoop = false; // Valid input, exit the choose loop
					}
					// Key press was: C (and only if the game is currently in progress and thus able to be continued)
					if (game->getProg() && choice.Event.KeyEvent.wVirtualKeyCode == 0x43)
					{
						invalidKey = false; // Break the key validation loop
						continueGame = true; // Set flag to continue
						play = true; // Set flag to play
						chooseLoop = false; // Valid input, exit the choose loop
					}
				}
			}
		}

		// User has entered a valid menu option, either to exit or to play the game
		if (play)
		{ // User has chosen to play a game

			bool newGame{ false }; // Start a new game? (false = no, true = yes)

			// See if the user has made any progress within the current game session
			if ((game->getProg() && !continueGame) || (invest::money != 2000 && !continueGame))
			{ // A game is in progress, but the user has not chosen to continue (i.e. they selected New or Load game)
				// Display a warning to the user that this will overwrite any current progress
				std::cout << std::endl << " " << setCol(WHITE, RED) << " WARNING: " << setCol(RED) << " This will reset any unsaved progress, consider continuing instead!" << std::endl << setCol(RED) << "      Press " << setCol(WHITE, RED) << " Y " << setCol(RED) << " to overwrite, or " << setCol(WHITE, RED) << " N " << setCol(RED) << " to carry on with your current game:" << setCol(WHITE) << " ";

				// Loop until they have given a valid input
				bool overwriteLoop{ true };
				while (overwriteLoop)
				{
					INPUT_RECORD irInput = getKeyPress(); // Get their input
					if (irInput.Event.KeyEvent.bKeyDown)
					{
						switch (irInput.Event.KeyEvent.wVirtualKeyCode)
						{
						case 0x59: // Key press was: Y
						{ // User has agreed to overwrite and start a new game
							if (load)
							{ // User selected '2' and wants to load their files
								try
								{ // Try to load the file
									game->loadFile();
								}
								catch (std::runtime_error err)
								{ // Error, file could not be found or loaded
									// Display the error message
									std::cout << std::endl << setCol(RED) << err.what() << setCol(WHITE) << std::endl;
									newGame = true; // Just start a new game instead
									Sleep(2000);
								}
							}
							else
							{ // User selected '1' and wants to start a fresh game
								newGame = true;
							}
							overwriteLoop = false; // Input was valid, exit loop
						}
						break;
						case 0x4e: // Key press was: N
						{ // User does not want to overwrite their progress, so just continue
							overwriteLoop = false; // Input was valid, exit loop
						}
						break;
						}
					}
				}
			}
			else if (load)
			{ // User has just started the program and wishes to load previously saved progress
				try
				{ // Try to load the file
					game->loadFile();
				}
				catch (std::runtime_error err)
				{ // Error, file could not be found or loaded
					// Display the error message
					std::cout << std::endl << setCol(RED) << err.what() << setCol(WHITE) << std::endl;
					newGame = true; // Just start a new game instead
					Sleep(2000);
				}
			}

			// User chose to start a new game
			if (newGame)
			{ // Reset the game, call the reset static functions
				game->resetProg();
				game->resetCargoList();
				game->resetNumLoaded();
				game->resetMoney();
			}

			// While the game is running
			while (play)
			{
				game->dispFrame(); // Display the cargo selection screen

				if (game->getDeparted())
				{ // User had loaded cargo, departed and the trip is finished, so display the results
					receipt results = receipt(); // Create a new instance of the receipt
					results.dispFrame(); // Display the receipt
					try
					{ // Try to save the progress
						game->saveFile();
					}
					catch (std::runtime_error err)
					{ // Error, file could not be saved
						// Display the error message
						std::cout << std::endl << setCol(RED) << err.what() << setCol(WHITE) << std::endl;
					}
					game->resetCargoList(); // Reset the loaded cargo list
					game->resetNumLoaded(); // Reset the number of cargo loaded
				}

				if (game->getToMenu())
				{ // User has chosen to exit the investment screen and return to the main menu
					play = false;
				}
			}
		}
	}
}

// Display the main title frame
void title::dispFrame()
{
	system("CLS"); // Clear the screen

	// Start the main frame for the title
	// Top line of the frame
	std::cout << setCol(RED) << " " << box::TLd;
	for (int i{ 0 }; i < 76; i++)
	{
		std::cout << box::Hd;
	}
	std::cout << box::TRd << std::endl;
	// The main text of the title,  along with all its colour formatting using our custom output modifier function
	// Text adapted after generation via: http://patorjk.com/software/taag/#p=display&f=Slant&t=Cargo%20CHAOS
	std::cout << " " << box::Vd << setCol(TEAL, BLACK) << "         ______                                                             " << setCol(RED) << box::Vd << std::endl;
	std::cout << " " << box::Vd << setCol(TEAL, BLACK) << "        / ____/___ __________ _____                                         " << setCol(RED) << box::Vd << std::endl;
	std::cout << " " << box::Vd << setCol(TEAL, BLACK) << "       / /   / __ `/ ___/ __ `/ __ \\                                        " << setCol(RED) << box::Vd << std::endl;
	std::cout << " " << box::Vd << setCol(TEAL, BLACK) << "      / /___/ /_/ / /  / /_/ / /_/ /     " << setCol(GREEN, BLACK) << "________  _____   ____  _____      " << setCol(RED) << box::Vd << std::endl;
	std::cout << " " << box::Vd << setCol(TEAL, BLACK) << "      \\____/\\__,_/_/   \\__, /\\____/     " << setCol(GREEN, BLACK) << "/ ____/ / / /   | / __ \\/ ___/      " << setCol(RED) << box::Vd << std::endl;
	std::cout << " " << box::Vd << setCol(TEAL, BLACK) << "                      /____/\t         " << setCol(GREEN, BLACK) << "/ /   / /_/ / /| |/ / / /\\__ \\       " << setCol(RED) << box::Vd << std::endl;
	std::cout << " " << box::Vd << setCol(TEAL, BLACK) << "\t                                " << setCol(GREEN, BLACK) << "/ /___/ __  / ___ / /_/ /___/ /       " << setCol(RED) << box::Vd << std::endl;
	std::cout << " " << box::Vd << setCol(GRAY, BLACK) << "\t   an Object-Oriented game      " << setCol(GREEN, BLACK) << "\\____/_/ /_/_/  |_\\____//____/        " << setCol(RED) << box::Vd << std::endl;
	std::cout << " " << box::Vd << setCol(GRAY, BLACK) << "\t            written in C++                                            " << setCol(RED) << box::Vd << std::endl;
	std::cout << " " << box::Vd << setCol(TEAL, BLACK) << "\t                                                                      " << setCol(RED) << box::Vd << std::endl;
	// Bottom line of the frame
	std::cout << " " << box::BLd << box::Td;
	for (int i{ 0 }; i < 75; i++)
	{ // Connect to different parts of the menu, depending on the position along the bottom of the frame
		if (i == 15)
		{
			std::cout << box::Td;
		}
		else if (i == 31 && invest::gameProg)
		{ // The ` C) Continue ' menu option needs to be connected too
			std::cout << box::Td;
		}
		else
		{
			std::cout << box::Hd;
		}
	}
	std::cout << box::BRd << std::endl;
	// Start drawing the main menu options
	// Menu option 1: New game
	std::cout << setCol(RED) << "  " << box::Vd << setCol(TEAL) << " 1)  New Game  " << setCol(RED) << box::Vd;
	// Menu option C: Continue
	if (invest::gameProg)
	{ // Add a continue option if a game is in progress
		std::cout << setCol(TEAL) << " C)  Continue  " << setCol(RED) << box::Vd << std::endl;
	}
	else
	{
		std::cout << std::endl;
	}
	std::cout << "  " << box::Rd;
	for (int i{ 0 }; i < 15; i++)
	{
		std::cout << box::H;
	}
	if (invest::gameProg)
	{ // A different connection needs to be drawn
		std::cout << box::Pd;
		for (int i{ 0 }; i < 15; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::BRd;
	}
	else
	{
		std::cout << box::Ld;
	}
	std::cout << setCol(WHITE) << std::endl;
	// Menu option 2: Load game
	std::cout << setCol(RED) << "  " << box::Vd << setCol(TEAL) << " 2)  Load Game " << setCol(RED) << box::Vd << setCol(WHITE) << "\t\t     " << box::TL << box::H << box::H << box::H << box::TR << "         .____________________." << std::endl;
	std::cout << setCol(RED) << "  " << box::Rd;
	for (int i{ 0 }; i < 15; i++)
	{
		std::cout << box::H;
	}
	std::cout << box::Ld << "\t\t     " << setCol(WHITE) << box::V << setCol(GREEN) << "GO!" << setCol(WHITE) << box::V << "    ___, |" << setCol(GRAY, DTEAL) << "                    " << setCol(WHITE) << "|" << std::endl;
	// Menu option 3: About
	std::cout << setCol(RED) << "  " << box::Vd << setCol(TEAL) << " 3)  About     " << setCol(RED) << box::Vd << "\t\t     " << setCol(WHITE) << box::BL << box::H << box::T << box::H << box::BR << "   /_| | |" << setCol(YELLOW, DTEAL) << "    by Zac Baker!" << setCol(GRAY, DTEAL) << "   " << setCol(WHITE) << "|" << std::endl;
	std::cout << setCol(RED) << "  " << box::Rd;
	for (int i{ 0 }; i < 15; i++)
	{
		std::cout << box::H;
	}
	std::cout << box::Ld << "\t\t       " << setCol(WHITE) << box::V << "    |    |_|" << setCol(GRAY, DTEAL) << "                    " << setCol(WHITE) << "|" << std::endl;
	// Menu option 4: Reference
	std::cout << setCol(RED) << "  " << box::Vd << setCol(TEAL) << " 4)  Reference " << setCol(RED) << box::Vd << "\t\t      " << setCol(WHITE) << box::H << box::Tu << box::H << "   `-O----O-O' `" << box::H << box::H << box::H << box::H << box::H << "'   `O`O'-' " << std::endl;
	std::cout << setCol(RED) << "  " << box::Rd;
	for (int i{ 0 }; i < 15; i++)
	{
		std::cout << box::H;
	}
	std::cout << box::Ld << "                Please ensure your command prompt is over" << setCol(WHITE) << std::endl;
	// Menu option 5: Exit
	std::cout << setCol(RED) << "  " << box::Vd << setCol(TEAL) << " 5)  Exit      " << setCol(RED) << box::Vd << "                  80 characters wide and 25 lines long!" << std::endl;
	std::cout << setCol(RED) << "  " << box::BLd;
	for (int i{ 0 }; i < 15; i++)
	{
		std::cout << box::Hd;
	}
	std::cout << box::BRd << setCol(WHITE) << std::endl;

	// Ask the user for their choice
	std::cout << setCol(TEAL) << " Please enter your choice using your keyboard!" << setCol(WHITE);
}

// Display the farewell frame
void title::dispFarewell()
{
	system("CLS"); // Clear the screen

	// Start outputting the pretty picture :)
	// The ASCII sun has been adapted from the one found at: http://www.chris.com/ascii/joan/www.geocities.com/SoHo/7373/celestal.html
	std::cout << std::endl;
	std::cout << "                                " << setCol(RED) << box::SL << box::SL << box::SL << box::S << box::S << box::S << box::S << box::S << box::S << box::S << box::S << box::S << box::S << box::SR << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << setCol(YELLOW) << "       \\ | / ,              " << setCol(RED) << box::SL << box::SL << box::SL << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << box::SL << box::S << box::S << box::S << box::S << box::S << box::S << box::S << box::S << box::SR << box::SR << box::SR << setCol(RED) << box::SR << box::SR << box::SR << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << setCol(YELLOW) << "     \"-.###.-            " << setCol(RED) << box::SL << box::SL << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << box::SL << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << box::SL << box::S << box::S << box::S << box::S << box::S << box::S << box::SR << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << box::SR << box::SR << box::SR << setCol(RED) << box::SR << box::SR << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << setCol(YELLOW) << "    -==#####==-        " << setCol(RED) << box::SL << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << box::SL << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << box::SL << box::S << box::S << box::S << box::S << box::SR << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << box::SR << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << box::SR << box::SR << setCol(RED) << box::SR << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << setCol(YELLOW) << "     .-`###'-.       " << setCol(RED) << box::SL << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << box::SL << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << box::SL << box::S << box::S << box::SR << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << box::SR << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << box::SR << setCol(RED) << box::SR << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << setCol(YELLOW) << "       / | \\       " << setCol(RED) << box::SL << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << box::SL << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << box::SL << box::SR << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << box::SR << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << box::SR << setCol(RED) << box::SR << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "                  " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << box::SL << box::SL << box::SL << setCol(WHITE) << "    " << setCol(BLUE) << box::SR << box::SR << box::SR << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "                " << setCol(RED) << box::SL << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << box::SL << box::SL << setCol(WHITE) << "            " << setCol(BLUE) << box::SR << box::SR << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "               " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << box::SL << setCol(WHITE) << "                  " << setCol(BLUE) << box::SR << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "              " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << box::SL << setCol(WHITE) << "                      " << setCol(BLUE) << box::SR << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "             " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << box::SL << setCol(WHITE) << "                          " << setCol(BLUE) << box::SR << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "            " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(WHITE) << "                              " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "           " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << box::SL << setCol(WHITE) << "                                " << setCol(BLUE) << box::SR << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "          " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(YELLOW) << "    _____ _                 _       " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "         " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(YELLOW) << "    |_   _| |__   __ _ _ __ | | __    " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "        " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(YELLOW) << "       | | | '_ \\ / _` | '_ \\| |/ /     " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "       " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(YELLOW) << "        | | | | | | (_| | | | |   <       " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "      " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(YELLOW) << "         |_|_|_| |_|\\__,_|_| |_|_|\\_\\       " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "      " << setCol(RED) << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(YELLOW) << "             \\ \\ / /__  _   _| |              " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << setCol(WHITE) << std::endl;
	std::cout << "     " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(YELLOW) << "               \\ | / _ \\| | | | |               " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "     " << setCol(RED) << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(YELLOW) << "                 | | (_) | |_| |_|                " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << setCol(WHITE) << std::endl;
	std::cout << "    " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(YELLOW) << "                  |_|\\___/ \\__,_(_)                 " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "   " << setCol(RED) << box::SL << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << setCol(GREEN) << box::SL << box::SL << setCol(TEAL) << box::SL << setCol(BLUE) << box::SL << box::SL << setCol(WHITE) << "                                                      " << setCol(BLUE) << box::SR << box::SR << setCol(TEAL) << box::SR << setCol(GREEN) << box::SR << box::SR << setCol(YELLOW) << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << box::SR << setCol(WHITE) << std::endl;
	std::cout << "   " << setCol(RED) << box::SL << setCol(PINK) << box::SL << box::SL << setCol(YELLOW) << box::SL << box::SL << setCol(GREEN) << box::SL << setCol(TEAL) << box::SL << box::SL << setCol(BLUE) << box::SL << setCol(WHITE) << "                                                        " << setCol(BLUE) << box::SR << setCol(TEAL) << box::SR << box::SR << setCol(GREEN) << box::SR << setCol(YELLOW) << box::SR << box::SR << setCol(PINK) << box::SR << box::SR << setCol(RED) << box::SR << setCol(WHITE);
	// `Thank You!' text adapted after generation via: http://patorjk.com/software/taag/#p=display&f=Big&t=Thank%0AYou!

	// Pause for 2 seconds
	Sleep(2000);
}


// Display the about frame
void about::dispFrame()
{
	system("CLS"); // Clear the screen

	// Display the text at the top of the screen
	std::cout << "  " << setCol(PINK) << box::TLd;
	for (int i{ 0 }; i < 9; i++)
	{
		std::cout << box::Hd;
	}
	std::cout << box::TRd << std::endl;
	std::cout << "  " << box::Vd << setCol(GREEN) << "  ABOUT  " << setCol(PINK) << box::Vd << std::endl;
	std::cout << "  " << box::BLd;
	for (int i{ 0 }; i < 9; i++)
	{
		std::cout << box::Hd;
	}
	std::cout << box::BRd << std::endl;
	std::cout << std::endl;

	// The Program
	std::cout << setCol(GREEN) << " > The Program" << std::endl;
	std::cout << setCol(GRAY) << "   This program is a little `simulation' of what it might be like to run" << std::endl << "    your own shipping company!" << std::endl;
	std::cout << "   You will start off with " << setCol(YELLOW) << box::GBP << "2000" << setCol(GRAY) << " to invest as you please, and who knows," << std::endl << "    maybe you can strike it rich in the perilous world of container chaos!" << std::endl;

	// Transport Methods
	std::cout << std::endl << setCol(GREEN) << " > Transport Methods" << std::endl;
	std::cout << setCol(GRAY) << "   There are 3 modes of transport, each varying with speed and capacity:" << std::endl;
	std::cout << "\t" << box::TL; // Start the top of the table
	for (int i{ 0 }; i < 38; i++)
	{
		std::cout << box::H;
	}
	std::cout << box::TR << std::endl;
	std::cout << "\t" << box::V << setCol(WHITE) << "  Mode       Speed     Max. Capacity  " << setCol(GRAY) << box::V << std::endl; // Headers of the table
	std::cout << "\t" << box::BL;
	for (int i{ 0 }; i < 10; i++)
	{
		std::cout << box::H;
	}
	std::cout << box::T;
	for (int i{ 0 }; i < 10; i++)
	{
		std::cout << box::H;
	}
	std::cout << box::T;
	for (int i{ 0 }; i < 16; i++)
	{
		std::cout << box::H;
	}
	std::cout << box::BR << std::endl; // Full table contents
	std::cout << setCol(TEAL) << "\t   Ships   " << setCol(GRAY) << box::V << "  Slow    " << box::V << "      52" << std::endl;
	std::cout << setCol(TEAL) << "\t   Trucks  " << setCol(GRAY) << box::V << "  Medium  " << box::V << "      22" << std::endl;
	std::cout << setCol(TEAL) << "\t   Planes  " << setCol(GRAY) << box::V << "  Fast    " << box::V << "      10" << std::endl;
	std::cout << "\t ";
	for (int i{ 0 }; i < 10; i++)
	{
		std::cout << box::H;
	}
	std::cout << box::Tu;
	for (int i{ 0 }; i < 10; i++)
	{
		std::cout << box::H;
	}
	std::cout << box::Tu;
	for (int i{ 0 }; i < 16; i++)
	{
		std::cout << box::H;
	} // End of the table
	std::cout << std::endl;
	std::cout << "   Each mode of transport is a balance between speed and capacity." << std::endl << "   The faster the transport, the less time it has for things to go wrong," << std::endl << "    but the less bulk it can take per trip!" << std::endl;

	// Container Types
	std::cout << std::endl << setCol(GREEN) << " > Container Types" << std::endl;
	std::cout << setCol(GRAY) << "   There are 5 types of containers that can be tranported via any method:" << std::endl;
	std::cout << "   " << box::arrR << " " << setCol(DGREEN, WHITE) << " Normal " << setCol(GRAY) << " Nothing special, just your ISO-standardised box container." << std::endl;
	std::cout << "   " << box::arrR << " " << setCol(DTEAL, WHITE) << " Flat " << setCol(GRAY) << " With only a base, there isn't anything too bulky for this " << std::endl << "      type of container." << std::endl;
	std::cout << "   " << box::arrR << " " << setCol(DRED, WHITE) << " Refrigerated " << setCol(GRAY) << " Adapted to contain perishable goods, these containers will" << std::endl << "      make sure your winter strawberries get there just as fresh." << std::endl;
	std::cout << "   " << box::arrR << " " << setCol(DPINK, WHITE) << " Liquid " << setCol(GRAY) << " Is your material above its triple point? If so, it can be" << std::endl << "      transported in one of these nifty vessels." << std::endl;
	std::cout << "   " << box::arrR << " " << setCol(DYELLOW, WHITE) << " Hazardous " << setCol(GRAY) << " Watch out, these containers glow!" << std::endl << "      Only the most dangerous (yet profitable) items are transported in these." << std::endl;

	// Random Events
	std::cout << std::endl << setCol(GREEN) << " > Random Events" << std::endl;
	std::cout << setCol(GRAY) << "   During your journey, you may encounter some special and exciting events!" << std::endl;
	std::cout << "   Don't be alarmed if something strange happens to you or your cargo, but note" << std::endl;
	std::cout << "    that these events may have ..unforeseen consequences.. on the value" << std::endl;
	std::cout << "    of your containers. The text on screen will tell you what's going on." << std::endl;
	std::cout << "   Don't like to gamble with your investment?" << std::endl;
	std::cout << "   - Pick a quicker mode of transport to reduce your exposure to Tyche/Fortuna." << std::endl;

	// Saving
	std::cout << std::endl << setCol(GREEN) << " > Saving" << std::endl;
	std::cout << setCol(GRAY) << "   You can save your progress (before loading cargo) in the cargo selection menu    The game is also saved automatically after every trip." << std::endl;
	std::cout << "   If you wouldn't like a file to be created on your system," << std::endl;
	std::cout << "    you can decline the save file when prompted!" << std::endl;
	std::cout << "   You can continue a game in progress by pressing " << setCol(DRED, WHITE) << " C " << setCol(GRAY) << " on the main menu," << std::endl;
	std::cout << "    else loading or starting a new game will overwrite any un-saved progress." << std::endl;

	// Reference
	std::cout << std::endl << setCol(GREEN) << " > Container Reference" << std::endl;
	std::cout << setCol(GRAY) << "   From the menu, select option " << setCol(TEAL) << "4) Reference" << setCol(GRAY) << " to view specific information" << std::endl;
	std::cout << "    about each of the containers and use the fancy `math mode', in which you" << std::endl;
	std::cout << "    can add together the attributes of two containers, or multiply a container" << std::endl;
	std::cout << "    by an integer factor to see what a transport full of them" << std::endl;
	std::cout << "    might net you in " << setCol(YELLOW) << "profit" << setCol(GRAY) << "!" << std::endl;
	std::cout << "   To showcase the implementation of the move constructor and" << std::endl;
	std::cout << "    assignment operator, please try out the `swap mode'!" << std::endl;


	// Propmt to scroll up, and instructions on how to return to the main menu
	std::cout << std::endl << std::endl << setCol(TEAL) << "\t\t\t\tPLEASE SCROLL UP!" << std::endl;
	std::cout << std::endl << setCol(GRAY) << "Press Enter or Esc to go back to the main menu...";

	// Run until a valid input is entered
	bool loopAbout{ true };
	while (loopAbout)
	{
		INPUT_RECORD irInput = getKeyPress(); // Get their input
		if (irInput.Event.KeyEvent.bKeyDown && (irInput.Event.KeyEvent.wVirtualKeyCode == VK_RETURN || irInput.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE))
		{ // Key press was Return
			loopAbout = false; // Break the loop so we can return to the main menu
		}
	}
}


// Memeber function to change the current container selection along the row
void reference::loopSelection(const int incrementRow)
{
	if (currTopRow)
	{ // Currently on the top row with transport types
		currentSel.first += incrementRow; // Increment

		// Validate the new value
		if (currentSel.first > 2)
		{ // The top row's index == 3, so loop it back to 0
			currentSel.first -= 3;
		}
		if (currentSel.first < 0)
		{ // The top row's index == -1, so loop it back to 2
			currentSel.first += 3;
		}
	}
	else
	{ // Currently on the bottom row with the container types
		currentSel.second += incrementRow; // Increment

		// Validate the new value
		if (currentSel.second > 4)
		{ // The bottom row's index == 5, so loop it back to 0
			currentSel.second -= 5;
		}
		if (currentSel.second < 0)
		{ // The bottom row's index == -1, so loop it back to 4
			currentSel.second += 5;
		}
	}
}

// Copy constructor for the reference page
reference::reference(reference& ref) : reference() // <- Delagating the default constructor
{
	// Assign the values from reference object
	currentSel = ref.currentSel;
	leftSel = ref.leftSel;
	rightSel = ref.rightSel;
	currTopRow = ref.currTopRow;
	addMode = ref.addMode;
	swapMode = ref.swapMode;
	factorVal = ref.factorVal;
}

// Move constructor for the reference page
reference::reference(reference&& ref) : reference() // <- Delegating the default constructor
{
	// Swap the values of `this' and `ref'
	std::swap(currentSel, ref.currentSel);
	std::swap(leftSel, ref.leftSel);
	std::swap(rightSel, ref.rightSel);
	std::swap(currTopRow, ref.currTopRow);
	std::swap(addMode, ref.addMode);
	std::swap(mathMode, ref.mathMode);
	std::swap(swapMode, ref.swapMode);
	std::swap(factorVal, ref.factorVal);
}

// Move assignment operator for ship-based normal containers
reference& reference::operator=(reference&& ref)
{
	// Prevent self-assignment of the reference object
	if (&ref == this) { return *this; }

	// Swap the values of `this' and `ref'
	std::swap(currentSel, ref.currentSel);
	std::swap(leftSel, ref.leftSel);
	std::swap(rightSel, ref.rightSel);
	std::swap(currTopRow, ref.currTopRow);
	std::swap(addMode, ref.addMode);
	std::swap(mathMode, ref.mathMode);
	std::swap(swapMode, ref.swapMode);
	std::swap(factorVal, ref.factorVal);

	// Return the reference object with swapped values
	return *this;
}

// Display the container reference frame
void reference::dispFrame()
{
	// Create our reference container list
	std::vector<std::vector<container*>> referenceContainers; // 2D container vector, top vector contains 3 vectors (sea, land and air) each containing the 5 different types of containers
	referenceContainers.insert(referenceContainers.end(), { new sea::normal(), new sea::flat(), new sea::fridge(), new sea::liquid(), new sea::hazard() }); // Insert the sea containers
	referenceContainers.insert(referenceContainers.end(), { new land::normal(), new land::flat(), new land::fridge(), new land::liquid(), new land::hazard() }); // Insert the land containers
	referenceContainers.insert(referenceContainers.end(), { new air::normal(), new air::flat(), new air::fridge(), new air::liquid(), new air::hazard() }); // Insert the air containers

	// Create a vector for the conteriner text to be used in math mode
	std::vector<std::vector<std::string>> textContainers;
	textContainers.insert(textContainers.end(), { "Ship Normal", "Ship Flat", "Ship Refrigerated", "Ship Liquid", "Ship Hazardous" }); // Insert the Ship's container text
	textContainers.insert(textContainers.end(), { "Truck Normal", "Truck Flat", "Truck Refrigerated", "Truck Liquid", "Truck Hazardous" }); // Insert the Truck's container text
	textContainers.insert(textContainers.end(), { "Plane Normal", "Plane Flat", "Plane Refrigerated", "Plane Liquid", "Plane Hazardous" }); // Insert the Plane's container text


	// Run until a valid input is entered
	bool loopRef{ true };
	while (loopRef)
	{
		system("CLS"); // Clear the screen

		// Display the text at the top of the screen
		std::cout << "  " << setCol(PINK) << box::TLd;
		for (int i{ 0 }; i < 13; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::TRd << std::endl;
		std::cout << "  " << box::Vd << setCol(GREEN) << "  REFERENCE  " << setCol(PINK) << box::Vd;
		if (swapMode)
		{ // If we have just previous swapped instances, display this to the user!
			std::cout << "  " << setCol(DYELLOW, WHITE) << " SWAPPED! " << setCol(PINK);
		}
		std::cout << std::endl;
		std::cout << "  " << box::BLd;
		for (int i{ 0 }; i < 13; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::BRd << setCol(WHITE) << std::endl;
		std::cout << std::endl;

		// Display the message to the user
		std::cout << " Choose a container type to view it's properties and information!" << std::endl;
		std::cout << "  Keys: " << setCol(GRAY) << box::arrL << box::arrUD << box::arrR << setCol(WHITE) << " navigation, " << setCol(GRAY) << "Enter" << setCol(WHITE) << " or " << setCol(GRAY) << "Esc" << setCol(WHITE) << " return to the main menu, " << std::endl;
		std::cout << "       " << setCol(GRAY) << "M" << setCol(WHITE) << " to toggle " << setCol(GREEN) << "math mode" << setCol(WHITE) << " on or off, " << setCol(GRAY) << "S" << setCol(WHITE) << " to toggle " << setCol(GREEN) << "swap mode" << setCol(WHITE) << std::endl;

		// First print out the different transport types
		std::cout << std::endl;
		// Indicate if the user is currently choosing between transport methods
		if (currTopRow)
		{ // They are
			std::cout << setCol(BLACK, TEAL) << "->" << setCol(WHITE);
		}
		else
		{ // They are not
			std::cout << "  ";
		}
		std::cout << "  ";

		// Indicate what the current transport selection is
		if (currentSel.first == 0)
		{ // Currently on `Ship'
			std::cout << setCol(TEAL) << ">> ";
		}
		else
		{
			std::cout << "   ";
		}
		std::cout << "Ship";
		if (currentSel.first == 0)
		{
			std::cout << " <<" << setCol(WHITE);
		}
		else
		{
			std::cout << "   ";
		}
		if (currentSel.first == 1)
		{ // Currently on `Truck'
			std::cout << setCol(TEAL) << ">> ";
		}
		else
		{
			std::cout << "   ";
		}
		std::cout << "Truck";
		if (currentSel.first == 1)
		{
			std::cout << " <<" << setCol(WHITE);
		}
		else
		{
			std::cout << "   ";
		}
		if (currentSel.first == 2)
		{ // Currently on `Plane'
			std::cout << setCol(TEAL) << ">> ";
		}
		else
		{
			std::cout << "   ";
		}
		std::cout << "Plane";
		if (currentSel.first == 2)
		{
			std::cout << " <<" << setCol(WHITE);
		}
		else
		{
			std::cout << "   ";
		}
		std::cout << std::endl << std::endl; // Pad lines


		// Now print out the different container types below the selected transport
		// Indicate if the user is currently choosing between container types
		if (!currTopRow)
		{ // They are
			std::cout << setCol(BLACK, TEAL) << "->" << setCol(WHITE);
		}
		else
		{ // They are not
			std::cout << "  ";
		}
		std::cout << "  ";

		// Indicate what the current container selection is
		if (currentSel.second == 0)
		{ // Currently on `Normal'
			std::cout << setCol(TEAL) << ">> ";
		}
		else
		{
			std::cout << "   ";
		}
		std::cout << "Normal";
		if (currentSel.second == 0)
		{
			std::cout << " <<" << setCol(WHITE);
		}
		else
		{
			std::cout << "   ";
		}
		if (currentSel.second == 1)
		{ // Currently on `Flat'
			std::cout << setCol(TEAL) << ">> ";
		}
		else
		{
			std::cout << "   ";
		}
		std::cout << "Flat";
		if (currentSel.second == 1)
		{
			std::cout << " <<" << setCol(WHITE);
		}
		else
		{
			std::cout << "   ";
		}
		if (currentSel.second == 2)
		{ // Currently on `Refrigerated'
			std::cout << setCol(TEAL) << ">> ";
		}
		else
		{
			std::cout << "   ";
		}
		std::cout << "Refrigerated";
		if (currentSel.second == 2)
		{
			std::cout << " <<" << setCol(WHITE);
		}
		else
		{
			std::cout << "   ";
		}
		if (currentSel.second == 3)
		{ // Currently on `Liquid'
			std::cout << setCol(TEAL) << ">> ";
		}
		else
		{
			std::cout << "   ";
		}
		std::cout << "Liquid";
		if (currentSel.second == 3)
		{
			std::cout << " <<" << setCol(WHITE);
		}
		else
		{
			std::cout << "   ";
		}
		if (currentSel.second == 4)
		{ // Currently on `Hazardous'
			std::cout << setCol(TEAL) << ">> ";
		}
		else
		{
			std::cout << "   ";
		}
		std::cout << "Hazardous";
		if (currentSel.second == 4)
		{
			std::cout << " <<" << setCol(WHITE);
		}
		else
		{
			std::cout << "   ";
		}
		std::cout << std::endl << std::endl; // Pad lines


		// Check if we're in the continer math mode
		if (mathMode)
		{ // Need to change the information text and display a different contai{ner
			// Display instructions
			std::cout << setCol(BLACK, WHITE) << " Maths:" << setCol(WHITE) << " Press " << setCol(BLUE, GRAY) << " L " << setCol(WHITE) << " or " << setCol(RED, GRAY) << " R " << setCol(WHITE) << " to assign the currently selected container to the left" << std::endl;
			std::cout << "     or right of the operator. The operator can be changed using the keyboard," << std::endl;
			std::cout << "     you can choose between: " << setCol(TEAL, DBLUE) << " + " << setCol(WHITE) << " to add the two, or " << setCol(TEAL, DBLUE) << " * " << setCol(WHITE) << " to multiply the" << std::endl;
			std::cout << "     Left container by a integer number (press * again to reset the number)" << setCol(WHITE) << std::endl << std::endl;
			// Display the current container equation
			std::cout << " " << setCol(BLUE, GRAY) << " " << textContainers.at(leftSel.first).at(leftSel.second).c_str() << " " << setCol(TEAL);

			if (addMode)
			{ // Display a + and the tag for the current container
				std::cout << " + " << " " << setCol(RED, GRAY) << " " << textContainers.at(rightSel.first).at(rightSel.second).c_str() << " " << setCol(TEAL) << " =" << std::endl << std::endl;

				// Add the two selected containers, using our overloaded operator+
				(*referenceContainers.at(leftSel.first).at(leftSel.second) + *referenceContainers.at(rightSel.first).at(rightSel.second)).dispMaths(std::cout);
			}
			else
			{ // Display a * and by what factor the container will be multipled by
				std::cout << " * " << setCol(RED, GRAY) << " " << factorVal << " " << setCol(TEAL) << " =" << std::endl << std::endl;

				// Multiply the left container by a scalar (factorVal), using the overloaded operator*
				(*referenceContainers.at(leftSel.first).at(leftSel.second) * factorVal).dispMaths(std::cout);
			}
		}
		else
		{ // No maths, just display the information for the currently selected container
			std::cout << *referenceContainers.at(currentSel.first).at(currentSel.second); // Display the container at the appropriate 2D vector location, using overloaded operator<<
		}

		// Logic to get any key presses
		bool invalidKey{ true };
		while (invalidKey)
		{ // Loop until we get a valid key input, otherwise the screen will refresh on every input!
			INPUT_RECORD irInput = getKeyPress(); // Get any user input
			if (irInput.Event.KeyEvent.bKeyDown)
			{
				if (irInput.Event.KeyEvent.wVirtualKeyCode == VK_RETURN || irInput.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE)
				{ // Key was Return or Escape
					invalidKey = false; // Break the key validation loop
					loopRef = false; // Exit the main loop to return to the main menu
				}
				if (irInput.Event.KeyEvent.wVirtualKeyCode == VK_UP || irInput.Event.KeyEvent.wVirtualKeyCode == VK_DOWN)
				{ // Key was Up or Down - user has switched the row, so toggle the current selection
					invalidKey = false; // Break the key validation loop
					currTopRow = !currTopRow; // Inverse the top row flag
				}
				if (irInput.Event.KeyEvent.wVirtualKeyCode == VK_RIGHT)
				{ // Key was Right - increment along the current row
					invalidKey = false; // Break the key validation loop
					loopSelection(1); // Loop selection to the right
				}
				if (irInput.Event.KeyEvent.wVirtualKeyCode == VK_LEFT)
				{ // Key was Left - decrement along the current row
					invalidKey = false; // Break the key validation loop
					loopSelection(-1); // Loop selection to the left
				}
				if (irInput.Event.KeyEvent.wVirtualKeyCode == 0x4D)
				{ // Key was M - toggle math mode
					invalidKey = false; // Break the key validation loop
					mathMode = !mathMode; // Toggle the math mode flag
				}
				if (irInput.Event.KeyEvent.wVirtualKeyCode == 0x53)
				{ // Key was S - show that the move constructor and move assignment operators work by exiting back to the main menu with the swapMode flag set
					invalidKey = false; // Break the key validation loop
					swapMode = true; // Set the swapMode flag to true
					loopRef = false; // Exit the main loop to return to the main menu
				}

				// Only if we're in math mode:
				if (mathMode && irInput.Event.KeyEvent.wVirtualKeyCode == 0x4C)
				{ // Key was L - assign the left container
					invalidKey = false; // Break the key validation loop
					leftSel = currentSel; // Reassign the selection
				}
				// Only if we're in both math mode and adding mode:
				if (mathMode && addMode && irInput.Event.KeyEvent.wVirtualKeyCode == 0x52)
				{ // Key was R - assign the right container
					invalidKey = false; // Break the key validation loop
					rightSel = currentSel; // Reassign the selection
				}
				if (mathMode && (irInput.Event.KeyEvent.wVirtualKeyCode == VK_ADD || irInput.Event.KeyEvent.wVirtualKeyCode == 0x50))
				{ // Key was + or P (might not have + key) - container paths should now be addition
					invalidKey = false; // Break the key validation loop
					addMode = true; // Assign the selected operator to `+'
				}
				if (mathMode && (irInput.Event.KeyEvent.wVirtualKeyCode == VK_MULTIPLY || irInput.Event.KeyEvent.wVirtualKeyCode == 0x54))
				{ // Key was * or T (might not have * key) - container maths should now be multiplication
					invalidKey = false; // Break the key validation loop
					addMode = false; // Assign the selected operator to `*'

					// Ask the user for the multiplication factor, and validate it
					int attempts{0};
					do
					{
						system("CLS"); // Clear the screen
						std::cout << std::endl; // Padding

						if (attempts > 0)
						{ // The user has just previously entered an invalid amount, tell them this!
							std::cout << setCol(RED) << "   Sorry but that is an invalid choice!";
							// Clear and ignore the invalid input
							std::cin.clear();
							std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
						}

						// Display the prompt message and information
						std::cout << std::endl << std::endl << setCol(GRAY) << " Please enter the factor you wish to multiply by (integer " << setCol(YELLOW) << "1" << setCol(GRAY) << "-" << setCol(YELLOW) << "100" << setCol(GRAY) << " only): " << setCol(YELLOW);
						std::cin >> factorVal; // Extract the value
						attempts++; // Increment the attempt counter
					}
					while (std::cin.fail() || factorVal < 0 || factorVal > 100); // Validation that it's an integer in the range of 1 - 100
				}
			}
		}
	}

	// Delete our vector of example containers
	for (auto iter = referenceContainers.begin(); iter < referenceContainers.end(); ++iter)
	{ // Iterate through the 3 top-level elements
		for (auto jter = iter->begin(); jter < iter->end(); ++jter)
		{ // Iterate through each of the 5 lower-level elements
			delete *jter; // Delete the instance
		}
		iter->clear(); // Clear the inner vectors
	}
	referenceContainers.clear(); // Clear the top vector
}
