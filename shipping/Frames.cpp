// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include "Frames.h"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <memory>
#include <random>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

#include "Containers.h"
#include "Events.h"


// Delare our static variables to be used across game sessions
std::vector<container*> invest::cargoList; // A list of loaded containers in the form of base class pointers
std::vector<int> invest::numLoaded = [] { std::vector<int> v; v.insert(v.begin(), {0, 0, 0, 0, 0});	return v; }(); // Custom lambda function to initialise the number loaded vector to 5 zeros
int invest::money; // The user's money
bool invest::gameProg(false); // Flag for if a game is in progress or not



// Default constructor for the investment frame
invest::invest() : currentSel{std::make_pair(0, 0)}, departed{false}, toMenu{false}, fileAgree{false}, neverStarted{true}
{
	// Initialise vectors using C++ 11 method
	transportNames = {"Ship", "Truck", "Plane"};
	containerNames = {"Normal", "Flat", "Fridge", "Liquid", "Hazard"};
	maxLoads = {52, 22, 10};
	costs = {100, 200, 300, 400, 800};

	// Initialise the default money amount, only once per instance of invest
	money = 2000;
}

// Destructor
invest::~invest()
{
	// Garbage collection
	for (auto iter = cargoList.begin(); iter < cargoList.end(); ++iter)
	{ // Iterate through the cargo list
		delete *iter; // Delete the current container
	}
	cargoList.clear(); // Clear the vector
}

/* Member functions to reset the static variables */
void invest::resetCargoList() // Reset the cargo list
{
	for (auto iter = cargoList.begin(); iter < cargoList.end(); ++iter)
	{ // Iterate through the cargo list
		delete *iter; // Delete the current container
	}
	cargoList.clear(); // Clear the vector
	cargoList.shrink_to_fit(); // Shrink the capacity of the vector back down to nothing, saving memory space
}

// Reset the number of containers loaded
void invest::resetNumLoaded()
{
	for (auto iter = numLoaded.begin(); iter < numLoaded.end(); ++iter)
	{ // Iterate through the vector
		*iter = 0; // Set each value to 0
	}
}

// Reset the game in progress flag
void invest::resetProg()
{
	gameProg = false; // Reset the flag
}

// Reset the user's money
void invest::resetMoney()
{
	money = 2000; // Default money is �2000
}

// Member function to display the investment frame
void invest::dispFrame()
{
	// Reset the menu flag
	toMenu = false;

	// Start the cargo selection menu loop
	bool loopCargoSel = {true};
	while (loopCargoSel)
	{
		system("CLS"); // Re-set the screen
		// Top of the display, page identifier and message
		std::cout << setCol(PINK) << "  " << box::TLd;
		for (int i{ 0 }; i < 10; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::TRd << std::endl;
		std::cout << "  " << box::Vd << setCol(GREEN) << "  INVEST  " << setCol(PINK) << box::Vd << setCol(GREEN) << "    Please invest in some cargo!" << std::endl;
		std::cout << "  " << setCol(PINK) << box::BLd;
		for (int i{ 0 }; i < 10; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::BRd << setCol(PINK) << "    ----------------------------" << setCol(WHITE) << std::endl;

		// If the user has not spent any money so far, the cargo list must be empty, so display that they have the option to save their game
		if (cargoList.empty())
		{
			std::cout << "\t\t\t\t\t\t  Press " << setCol(DRED, WHITE) << " S " << setCol(WHITE) << " to save your game!" << std::endl;
		}
		else // Some cargo has been loaded, display what is on the transport
		{
			std::cout << std::endl << "  Current cargo on your " << setCol(TEAL) << transportNames.at(cargoList.at(0)->getMedium()) << setCol(WHITE) << ":";
			for (auto iter = numLoaded.begin(); iter < numLoaded.end(); ++iter)
			{
				if (*iter > 0)
				{
					std::cout << setCol(YELLOW) << "  " << *iter << " " << containerNames.at(iter - numLoaded.begin());
				}
			}
			std::cout << setCol(WHITE);
		}

		// Display how much money the user has
		std::cout << std::endl << std::endl << "  Your remaining balance is: " << setCol(YELLOW) << box::GBP << money << setCol(GRAY) << std::endl << std::endl;

		// Display the different modes of transportation
		std::cout << setCol(WHITE) << "  Please choose a mode of transportation and the type of container:" << std::endl;
		std::cout << setCol(WHITE) << "  Keys: " << setCol(GRAY) << box::arrL << box::arrUD << box::arrR << setCol(WHITE) << " navigation, " << setCol(GRAY) << "Enter" << setCol(WHITE) << " confirm selection, " << setCol(GRAY) << "Esc" << setCol(WHITE) << " main menu" << std::endl;

		if (!cargoList.empty())
		{ // Then we need to confine the menu to the type of transport already chosen
			currentSel.first = cargoList.at(0)->getMedium();
		}

		// Print out the top row of the different transport types
		switch (currentSel.first)
		{ // Highlight the transport type dependent on what is currently selected
		case 0: // Current selection is `Ship'
		{
			std::cout << "\t" << setCol(TEAL) << box::TL;
			for (int i{ 0 }; i < 6; i++)
			{
				std::cout << box::H;
			}
			std::cout << box::TR << setCol(WHITE) << std::endl;
			std::cout << "\t" << setCol(TEAL) << box::V << setCol(TEAL) << " Ship " << setCol(TEAL) << box::V << setCol(WHITE);
			if (!cargoList.empty())
			{ // Gray out the now unselectable modes of transport
				std::cout << setCol(DRED);
			}
			std::cout << "         Truck          Plane " << std::endl;
			std::cout << "\t" << setCol(TEAL) << box::BL;
			for (int i{ 0 }; i < 6; i++)
			{
				std::cout << box::H;
			}
			std::cout << box::BR << setCol(WHITE);

			if (!cargoList.empty())
			{ // Some cargo has been loaded - give the user the option to depart with the currently loaded cargo!
				std::cout << "\t\t\t\t\t" << "Press " << setCol(DRED, WHITE) << " D " << setCol(WHITE) << " to depart" << std::endl;
				std::cout << "\t\t\t\t\t\t      " << "with the current cargo!";
			}
			else // Pad
			{
				std::cout << std::endl;
			}
		}
		break;
		case 1: // Current selection is `Truck'
		{
			std::cout << "\t               " << setCol(TEAL) << box::TL;
			for (int i{ 0 }; i < 7; i++)
			{
				std::cout << box::H;
			}
			std::cout << box::TR << setCol(WHITE) << std::endl;
			if (!cargoList.empty())
			{ // Gray out the now unselectable modes of transport
				std::cout << setCol(DRED);
			}
			std::cout << "\t  Ship         " << setCol(TEAL) << box::V << setCol(TEAL) << " Truck " << setCol(TEAL) << box::V << setCol(WHITE);
			if (!cargoList.empty())
			{ // Gray out the now unselectable modes of transport
				std::cout << setCol(DRED);
			}
			std::cout << "        Plane " << std::endl;
			std::cout << "\t               " << setCol(TEAL) << box::BL;
			for (int i{ 0 }; i < 7; i++)
			{
				std::cout << box::H;
			}
			std::cout << box::BR << setCol(WHITE);

			if (!cargoList.empty())
			{ // Some cargo has been loaded - give the user the option to depart with the currently loaded cargo!
				std::cout << "\t\t\t" << "Press " << setCol(DRED, WHITE) << " D " << setCol(WHITE) << " to depart" << std::endl;
				std::cout << "\t\t\t\t\t\t      " << "with the current cargo!";
			}
			else // Pad
			{
				std::cout << std::endl;
			}
		}
		break;
		case 2: // Current selection is `Plane'
		{
			std::cout << "\t                              " << setCol(TEAL) << box::TL;
			for (int i{ 0 }; i < 7; i++)
			{
				std::cout << box::H;
			}
			std::cout << box::TR << setCol(WHITE) << std::endl;
			if (!cargoList.empty())
			{ // Gray out the now unselectable modes of transport
				std::cout << setCol(DRED);
			}
			std::cout << "\t  Ship           Truck        " << setCol(TEAL) << box::V << setCol(TEAL) << " Plane " << setCol(TEAL) << box::V << setCol(WHITE) << std::endl;
			std::cout << "\t                              " << setCol(TEAL) << box::BL;
			for (int i{ 0 }; i < 7; i++)
			{
				std::cout << box::H;
			}
			std::cout << box::BR << setCol(WHITE);

			if (!cargoList.empty())
			{ // Some cargo has been loaded - give the user the option to depart with the currently loaded cargo!
				std::cout << "\t\t" << "Press " << setCol(DRED, WHITE) << " D " << setCol(WHITE) << " to depart" << std::endl;
				std::cout << "\t\t\t\t\t\t      " << "with the current cargo!";
			}
			else // Pad
			{
				std::cout << std::endl;
			}
		}
		break;
		}

		// Now print out the column of different container types below the selected transport
		std::cout << std::endl;
		// Add horizontal padding based on which column we want to print the container types in
		std::cout << "       ";
		for (int i{ 0 }; i < 15 * currentSel.first; i++)
		{
			std::cout << " ";
		}
		if (currentSel.second == 0)
		{ // Normal containers are currently selected
			std::cout << setCol(TEAL) << ">> " << setCol(TEAL);
		}
		else
		{
			std::cout << setCol(WHITE) << "   ";
		}
		std::cout << "Normal";
		if (currentSel.second == 0)
		{
			std::cout << setCol(TEAL) << " <<" << setCol(WHITE);
		}
		std::cout << std::endl << std::endl;
		std::cout << "       ";
		for (int i{ 0 }; i < 15 * currentSel.first; i++)
		{
			std::cout << " ";
		}
		if (currentSel.second == 1)
		{ // Flat-bed containers are currently selected
			std::cout << setCol(TEAL) << ">> " << setCol(TEAL);
		}
		else
		{
			std::cout << setCol(WHITE) << "   ";
		}
		std::cout << "Flat";
		if (currentSel.second == 1)
		{
			std::cout << setCol(TEAL) << " <<" << setCol(WHITE);
		}
		std::cout << std::endl << std::endl;
		std::cout << "       ";
		for (int i{ 0 }; i < 15 * currentSel.first; i++)
		{
			std::cout << " ";
		}
		if (currentSel.second == 2)
		{ // Refrigerated containers are currently selected
			std::cout << setCol(TEAL) << ">> " << setCol(TEAL);
		}
		else
		{
			std::cout << setCol(WHITE) << "   ";
		}
		std::cout << "Refrigerated";
		if (currentSel.second == 2)
		{
			std::cout << setCol(TEAL) << " <<" << setCol(WHITE);
		}
		std::cout << std::endl << std::endl;
		std::cout << "       ";
		for (int i{ 0 }; i < 15 * currentSel.first; i++)
		{
			std::cout << " ";
		}
		if (currentSel.second == 3)
		{ // Liquid containers are currently selected
			std::cout << setCol(TEAL) << ">> " << setCol(TEAL);
		}
		else
		{
			std::cout << setCol(WHITE) << "   ";
		}
		std::cout << "Liquid";
		if (currentSel.second == 3)
		{
			std::cout << setCol(TEAL) << " <<" << setCol(WHITE);
		}
		std::cout << std::endl << std::endl;
		std::cout << "       ";
		for (int i{ 0 }; i < 15 * currentSel.first; i++)
		{
			std::cout << " ";
		}
		if (currentSel.second == 4)
		{ // Hazardous containers are currently selected
			std::cout << setCol(TEAL) << ">> " << setCol(TEAL);
		}
		else
		{
			std::cout << setCol(WHITE) << "   ";
		}
		std::cout << "Hazardous";
		if (currentSel.second == 4)
		{
			std::cout << setCol(TEAL) << " <<" << setCol(WHITE);
		}
		std::cout << std::endl;

		bool invalidKey{ true };
		while (invalidKey)
		{ // Loop until we get a valid key input, otherwise the screen will refresh on every input!
			// Grab any user keyboard inputs
			INPUT_RECORD irInput = getKeyPress();
			if (irInput.Event.KeyEvent.bKeyDown)
			{ // Only trigger if a key has been depressed
				switch (irInput.Event.KeyEvent.wVirtualKeyCode)
				{
				case VK_ESCAPE: // Go back to the main menu
					invalidKey = false; // Break the key validation loop
					loopCargoSel = false; // Break the cargo selection loop
					departed = false; // Flag that the user did not depart
					toMenu = true; // Flag to return to the main menu
					break;
				case VK_RIGHT: // Move between transport types
					if (cargoList.empty())
					{ // Only change and update if the user is still able to switch transport modes
						invalidKey = false; // Break the key validation loop
						loopSelection(1, 0);
					}
					break;
				case VK_LEFT: // Move between transport types
					if (cargoList.empty())
					{ // Only change and update if the user is still able to switch transport modes
						invalidKey = false; // Break the key validation loop
						loopSelection(-1, 0);
					}
					break;
				case VK_UP: // Move between container types
					invalidKey = false; // Break the key validation loop
					loopSelection(0, -1);
					break;
				case VK_DOWN: // Move between container types
					invalidKey = false; // Break the key validation loop
					loopSelection(0, 1);
					break;
				case VK_RETURN: // Choose this item
					invalidKey = false; // Break the key validation loop
					if (money > 0)
					{ // The user can possibly purchase more cargo
						addCargo();
					}
					else
					{ // Don't change screen, instead display a warning message
						std::cout << setCol(RED) << "\t\t\t\t    You have no money, please depart instead!" << setCol(WHITE);
						Sleep(1000);
					}
					break;
				case 0x44: // Key was `D' - Depart with the currently loaded cargo
					invalidKey = false; // Break the key validation loop
					if (!cargoList.empty())
					{ // The some cargo has been loaded
						departed = true; // Flag that the cargo has departed
						if (cargoList.at(0)->getMedium() == 0)
						{ // Display the containers on a ship
							std::unique_ptr<frameSea> journeySea(new frameSea()); // Create a new instance of frameSea
							journeySea->dispFrame(); // Display the frame
							// `frameSea' instance will be destroyed when `journeySea' goes out of scope
						}
						else if (cargoList.at(0)->getMedium() == 1)
						{ // Display the containers on a truck
							std::unique_ptr<frameLand> journeyLand(new frameLand()); // Create a new instance of frameLand
							journeyLand->dispFrame(); // Display the frame
							// `frameLand' instance will be destroyed when `journeyLand' goes out of scope
						}
						else if (cargoList.at(0)->getMedium() == 2)
						{ // Display the containers on a plane
							std::unique_ptr<frameAir> journeyAir(new frameAir()); // Create a new instance of frameAir
							journeyAir->dispFrame(); // Display the frame
							// `frameAir' instance will be destroyed when `journeyAir' goes out of scope
						}
					}
					loopCargoSel = false; // Break the loop to return to the main menu
					break;
				case 0x53: // Key was `S' - Save the game
					if (cargoList.empty())
					{ // Only save progress if no cargo has been loaded, as only the amount of money is output, this prevents unshipped but purchased cargo from being lost if the user quits then loads!
						invalidKey = false; // Break the key validation loop
						try
						{ // Try to save the progress
							saveFile();
						}
						catch (std::runtime_error err)
						{ // Error, file could not be saved
							// Display the error message
							std::cout << std::endl << setCol(RED) << err.what() << setCol(WHITE) << std::endl;
						}
					}
				}
			}
		}
	}
}

// Member function to display the add cargo screen
void invest::addCargo()
{
	// Define variables
	int numContainers{0}; // The user's input
	int attemptAmount{false}; // How many times the input was invalid

	do
	{ // Run the screen until a valid input has been given
		system("CLS"); // Clear the screen

		// Display the text at the top of the screen
		std::cout << "  " << setCol(PINK) << box::TLd;
		for (int i{0}; i < 13; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::TRd << std::endl;
		std::cout << "  " << box::Vd << setCol(GREEN) << "  ADD CARGO  " << setCol(PINK) << box::Vd << setCol(GREEN) << "    Please select the amount of cargo!" << std::endl;
		std::cout << "  " << setCol(PINK) << box::BLd;
		for (int i{0}; i < 13; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::BRd << "    ----------------------------------" << setCol(WHITE) << std::endl;
		std::cout << std::endl;

		// Display to the user what container type and transport mode they selected
		std::cout << "  You have chosen to invest in " << setCol(TEAL) << transportNames.at(currentSel.first) << setCol(WHITE) << " based " << setCol(TEAL) << containerNames.at(currentSel.second) << setCol(WHITE) << " containers." << std::endl << std::endl;
		std::cout << "  You can add a further " << setCol(TEAL) << maxLoads.at(currentSel.first) - cargoList.size() << setCol(WHITE) << " containers to the " << setCol(TEAL) << transportNames.at(currentSel.first) << setCol(WHITE) << std::endl << std::endl;
		std::cout << "  With " << setCol(YELLOW) << box::GBP << money << setCol(WHITE) << " you can purchase a maximum of " << setCol(TEAL) << floor(money / costs.at(currentSel.second)) << " " << setCol(WHITE) << containerNames.at(currentSel.second) << " containers." << std::endl << std::endl;
		std::cout << setCol(GRAY) << "  Enter " << setCol(DRED, WHITE) << " 0 " << setCol(GRAY) << " to go back to the selection screen without adding anything!" << setCol(WHITE) << std::endl;

		if (attemptAmount)
		{ // The the user just previously provided an invalid input
			std::cout << std::endl << setCol(RED) << "  Sorry but that wasn't a valid amount" << setCol(WHITE) << std::endl;
			// Clear and ignore the invalid input
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		else // Pad
		{
			std::cout << std::endl << std::endl;
		}
		std::cout << "  How many would you like?: " << setCol(YELLOW);

		// Grab the input
		std::cin >> numContainers;

		attemptAmount = true; // Increment the attempt counter
	}
	while (std::cin.fail() || numContainers < 0 || static_cast<size_t>(numContainers) > static_cast<size_t>(maxLoads.at(currentSel.first)) - cargoList.size() || numContainers > floor(money / costs.at(currentSel.second)));
	// Validate the input ^ In order: cin is an integer, is positive, does not exceed the maximum load number of containers on the transport, would cost more than the amount of money they currently have

	if (numContainers != 0)
	{ // User has chosen a valid number of containers to add
		// Generate a random distribution for the ID tag
		std::random_device seed;
		std::mt19937 mtGen(seed());
		std::uniform_int_distribution<int> rDist(1000, 9999);

		for (int i{0}; i < numContainers; i++)
		{ // For the number of containers entered by the user...

			std::string idRand = std::to_string(rDist(mtGen)); // Set the ID tag

			// Push back the container to the base class pointer vector, based on which transport and type was selected
			switch (currentSel.first)
			{
				case 0: // Sea
					switch (currentSel.second)
					{
						case 0: // Normal
							cargoList.push_back(new sea::normal(idRand));
							break;
						case 1: // Flat
							cargoList.push_back(new sea::flat(idRand));
							break;
						case 2: // Fridge
							cargoList.push_back(new sea::fridge(idRand));
							break;
						case 3: // Liquid
							cargoList.push_back(new sea::liquid(idRand));
							break;
						case 4: // Hazardous
							cargoList.push_back(new sea::hazard(idRand));
							break;
					}
					break;
				case 1: // Truck
					switch (currentSel.second)
					{
						case 0: // Normal
							cargoList.push_back(new land::normal(idRand));
							break;
						case 1: // Flat
							cargoList.push_back(new land::flat(idRand));
							break;
						case 2: // Fridge
							cargoList.push_back(new land::fridge(idRand));
							break;
						case 3: // Liquid
							cargoList.push_back(new land::liquid(idRand));
							break;
						case 4: // Hazardous
							cargoList.push_back(new land::hazard(idRand));
							break;
					}
					break;
				case 2: // Plane
					switch (currentSel.second)
					{
						case 0: // Normal
							cargoList.push_back(new air::normal(idRand));
							break;
						case 1: // Flat
							cargoList.push_back(new air::flat(idRand));
							break;
						case 2: // Fridge
							cargoList.push_back(new air::fridge(idRand));
							break;
						case 3: // Liquid
							cargoList.push_back(new air::liquid(idRand));
							break;
						case 4: // Hazardous
							cargoList.push_back(new air::hazard(idRand));
							break;
					}
					break;
			}
		}

		// Deduct the cost from their money
		money -= numContainers * (*(cargoList.end() - 1))->getCost();

		// Count how many containers of each type have been loaded
		numLoaded.at(currentSel.second) += numContainers;
		gameProg = true; // Flag that the game is now in progress, as the user has spent some money to load cargo
		neverStarted = false; // Flag that some money has been spent at least once since the program started

		// Display a message indicating the addition of the cargo was sucessful
		std::cout << std::endl << std::endl << setCol(GREEN) << "  Added!" << setCol(WHITE);
		Sleep(666);
	}
}

// Member function to increment the container selection menu
void invest::loopSelection(const int incrementMode, const int incrementType)
{
	// Increment the values
	currentSel.first += incrementMode;
	currentSel.second += incrementType;

	// Test for out-of-bounds values
	if (currentSel.first > 2)
	{ // modeMenu = 3, so loop it back to 0
		currentSel.first -= 3;
	}
	if (currentSel.first < 0)
	{ // modeMenu = -1, so loop it back to 2
		currentSel.first += 3;
	}
	if (currentSel.second > 4)
	{ // typeMenu = 5, so loop it back to 0
		currentSel.second -= 5;
	}
	if (currentSel.second < 0)
	{ // typeMenu = -1, so loop it back to 4
		currentSel.second += 5;
	}
}

// Member function to save the current amount of money (i.e. progress) to a file
void invest::saveFile()
{
	// Ask for the user's permission to create a file
	if (!fileAgree)
	{ // User has not already agreed to waive this question
		// Display a warning message to the user
		std::cout << std::endl << " " << setCol(WHITE, RED) << " WARNING:" << setCol(RED) << " Saving your game will create a .txt file in the program's directory," << std::endl << setCol(WHITE) << "  " << setCol(RED) << "is this okay? " << setCol(WHITE, RED) << " Y " << setCol(RED) << " or " << setCol(WHITE, RED) << " N " << setCol(RED) << ": " << setCol(WHITE);

		// Loop until a valid input
		bool loopFile{true};
		while (loopFile)
		{
			INPUT_RECORD irInput = getKeyPress(); // Get any user input
			if (irInput.Event.KeyEvent.bKeyDown)
			{
				switch (irInput.Event.KeyEvent.wVirtualKeyCode)
				{
					case 0x59: // Key was `Y' - User has agreed to creating a file
						fileAgree = true; // Flag that they have given permission to create a file
						loopFile = false; // Break the loop
						break;
					case 0x4e: // Key was `N' - User does not want to create a file, so just continue
						loopFile = false; // Break the loop
						break;
				}
			}
		}
	}

	if (fileAgree)
	{ // If the user has given us permission, create the file
		// Display a message
		std::cout << setCol(GREEN) << "  SAVING";
		Sleep(444);
		std::cout << ".";
		Sleep(444);
		std::cout << ".";
		Sleep(444);
		std::cout << "." << setCol(WHITE);

		// Create the file to save to
		std::ofstream outFile("ContainerChaos-SaveFile.txt");
		// Validate that the file is open
		if (outFile.is_open())
		{
			outFile << money << std::endl; // Print out the money
			outFile.close(); // Close the file
		}
		else
		{ // Error: file could not be opened for saving
			throw std::runtime_error("  Sorry but there was a problem trying to save your game..."); // Throw a runtime error
		}

		// Display the rest of the message
		Sleep(444);
		std::cout << setCol(GREEN) << "  Saved!" << setCol(WHITE);
		Sleep(444);
	}
}

// Memeber function to load the save-game file
void invest::loadFile() const // Can actually be constant - variable money is static!
{
	// Open the file, if it exists
	std::ifstream inFile("ContainerChaos-SaveFile.txt");
	if (inFile.is_open())
	{
		std::string inMoneyStr;
		getline(inFile, inMoneyStr); // Grab the amount of money
		try
		{ // Try convert the money value
			money = stoi(inMoneyStr); // Set the money variable
		}
		catch (const std::invalid_argument)
		{ // No conversion could be performed
			throw std::runtime_error("  Error: No save file present! Starting a new game instead..."); // Throw a runtime error
		}
		catch (const std::out_of_range)
		{ // The value read is out of the range of representable values by an int
			throw std::runtime_error("  Error: No save file present! Starting a new game instead..."); // Throw a runtime error
		}

		inFile.close(); // Close the file

		// Do resets of static variables, because a loaded file must have been saved with no cargo loaded!
		resetProg();
		resetCargoList();
		resetNumLoaded();

		// Output the result to the user
		std::cout << std::endl << setCol(GREEN) << "  Loaded! You now have " << setCol(YELLOW) << box::GBP << money << setCol(WHITE);
		Sleep(2000);
	}
	else
	{ // No save file was present, or could not be opened
		throw std::runtime_error("  Error: No save file present! Starting a new game instead..."); // Throw a runtime error
	}
}


// Display the ship-based cargo frame
void frameSea::dispFrame()
{
	// Create a new instance of the random event class, using smart pointers
	std::unique_ptr<event> excitement(new event());
	// `event' instance will be destroyed when `excitement' goes out of scope

	int nBox{0}; // Total number of boxes to be transported
	// Custom lambda function to sum up a vector
	std::for_each(invest::numLoaded.begin(), invest::numLoaded.end(), [&](int n) { nBox += n; }); // Add element n to the counter for_each element

	// Define strings for the waves and map, both of which will be dynamic
	std::string wave1 = " )`.   )`.   )`.   )`.   )`.   )`.   )`.   )`.   )`.   )`.   )`.   )`.   )`.  ";
	std::string wave2 = "'   `-'   `-'   `-'   `-'   `-'   `-'   `-'   `-'   `-'   `-'   `-'   `-'   `-";
	std::string map1 = "  Start                                                             End  ";
	std::string map2 = "    X -------------------------------------------------------------- X   ";

	// Start the main `movie' loop, consisting of 62 frames
	for (int f{0}; f < 62; f++)
	{
		if (f % 3 == 0)
		{ // Every 3 steps, generate and test for a random event via our event class
			excitement->genEvent();
		}

		system("CLS"); // Clear the screen

		int currBox = 0; // Initialise a counter for which box we're on

		// Test how many rows are required (up to a maximum of 4 total rows, i.e. 3 additional rows)
		int topRow = nBox;
		int additionalRows{0};
		while (topRow > 13) // While the top row is too big
		{
			topRow -= 13; // Reduce the top row down to the maximum of 13...
			additionalRows++; // ...whilst counting the rows
		}

		// Pre-pad the top of the screen
		std::cout << setCol(GRAY) << std::endl << std::endl << std::endl;

		/* First row */
		// Top row
		if (additionalRows == 0)
		{ // Only a single row, so start printing out the top of the ship
			std::cout << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl << "       ___    ";
		}
		else
		{ // There are additional rows, so we need to wait until lower down to start plotting the ship
			for (int i{0}; i < 6 - additionalRows * 2; i++)
			{ // Pad the space, according to how many rows will be there
				std::cout << std::endl;
			}
			std::cout << "              ";
		}
		// Plot the top left corner of the first container
		std::cout << box::TL;
		for (int i{0}; i < topRow; i++)
		{ // Iterate through the number of boxes, displaying each in turn
			currBox++;
			std::cout << box::H << box::H << box::H;
			if (currBox == topRow)
			{ // The final box, so finish the edge corner
				std::cout << box::TR << std::endl;
			}
			else
			{ // There are more boxes, so connect it to the next box instead
				std::cout << box::T;
			}
		}

		// Middle of the top row of containers
		if (additionalRows == 0)
		{ // Continue drawing the ship
			std::cout << "      (   \\   ";
		}
		else
		{ // Pad the space
			std::cout << "              ";
		}
		// Draw |   | for each box
		std::cout << box::V;
		for (int i{0}; i < topRow; i++)
		{
			std::cout << "   " << box::V;
		}
		std::cout << std::endl;

		// Display any additional rows
		if (additionalRows != 0)
		{
			int currRow{0}; // Keep track of what row we're on
			while (currRow < additionalRows)
			{
				currRow++; // Increment the row
				if (currRow == additionalRows)
				{ // This is the bottom row, so start building the hull
					std::cout << "       ___    ";
				}
				else
				{ // This is not the lowest row, pad with void
					std::cout << "              ";
				}

				if (currRow == 1)
				{ // Top of the next row, connect to boxes above and correct for empty space where necessary
					std::cout << box::R;
					for (int i{0}; i < topRow; i++)
					{ // These boxes connect up
						if (i == 12)
						{ // Don't connect the final box
							break;
						}
						std::cout << box::H << box::H << box::H << box::P;
					}
					for (int i{0}; i < 12 - topRow; i++)
					{ // These boxes have empty space above
						std::cout << box::H << box::H << box::H << box::T;
					}
					if (topRow == 13)
					{ // Then the row above is completely full, need to have the top left corner connect
						std::cout << box::H << box::H << box::H << box::L << std::endl;
					}
					else
					{ // The top right corner is empty, so round off the edge
						std::cout << box::H << box::H << box::H << box::TR << std::endl;
					}
				}
				else
				{ // Top of the next row, which must be at least currRow == 2, so everything above must be conencted
					std::cout << box::R;
					for (int i{0}; i < 12; i++)
					{ // Connect all 13 containers
						std::cout << box::H << box::H << box::H << box::P;
					}
					std::cout << box::H << box::H << box::H << box::L << std::endl;
				}

				// Middle of the next row
				if (currRow == additionalRows)
				{ // Final row, so draw the ship
					std::cout << "      (   \\   ";
				}
				else
				{ // Fill void
					std::cout << "              ";
				}
				std::cout << box::V; // Draw the |   |
				for (int i{0}; i < 13; i++)
				{
					currBox++;
					std::cout << "   " << box::V;
				}
				std::cout << std::endl;

				if (currRow == additionalRows)
				{ // Bottom of the final row
					std::cout << "      |    `";
					std::cout << box::H << box::H << box::Tu;
					for (int i{0}; i < 13; i++)
					{
						std::cout << box::H << box::H << box::H << box::Tu;
					}
					std::cout << box::H << box::H << box::H << box::T;
				}
			}
		}
		else
		{ // Bottom of the only row
			std::cout << "      |    `";
			std::cout << box::H << box::H << box::Tu;
			for (int i{0}; i < nBox; i++)
			{
				std::cout << box::H << box::H << box::H << box::Tu;
			}
			for (int i{0}; i < 13 - nBox; i++)
			{
				std::cout << box::H << box::H << box::H << box::H;
			}
			std::cout << box::H << box::H << box::H << box::T;
		}
		// Draw the flag
		std::cout << box::H << box::T << box::H << box::H << box::H << box::H << box::H << box::TR << std::endl;

		// Draw the rest of the ship
		std::cout << "       \\                                                             /  " << box::V << setCol(RED) << box::SF << box::SF << box::SF << box::SF << box::SF << setCol(GRAY) << box::V << std::endl;
		std::cout << "        \\                                                           /   " << box::V << setCol(WHITE) << box::SF << box::SF << box::SF << box::SF << box::SF << setCol(GRAY) << box::V << std::endl;
		std::cout << "         \\  ";
		// Output the random event message
		// Calculate the amount of pre-padding required
		int pad = 53 - static_cast<int>(excitement->getExampleEvent().second.size()); // 53 spaces with no text
		int prePad = pad / 2; // Set the pre-pad as half of the characters to fill
		int postPad = pad / 2; // Likewise (note we are using interger rounding here, which will round down to the nearest integer

		if (prePad + postPad != pad)
		{ // If pad was an odd number, the rounding will have missed out one character which will detected and corrected for here
			postPad++;
		}
		// Output the pre-padding
		for (int i{0}; i < prePad; i++)
		{
			std::cout << " ";
		}
		// Alter the message's colour based on if the event was positive (green) or negative (red)
		if (excitement->getExampleEvent().first)
		{ // Positive event
			std::cout << setCol(GREEN);
		}
		else
		{ // Negative event
			std::cout << setCol(RED);
		}
		// Display the actual message, accessed from our events class
		std::cout << excitement->getExampleEvent().second << setCol(GRAY);

		// Output the post-padding
		for (int i{0}; i < postPad; i++)
		{
			std::cout << " ";
		}
		// Draw the rest of the ship's hull
		std::cout << "  /    " << box::V << setCol(BLUE) << box::SF << box::SF << box::SF << box::SF << box::SF << setCol(GRAY) << box::V << std::endl;
		std::cout << "          \\                                                       /     " << box::BL << box::H << box::H << box::H << box::H << box::H << box::BR << setCol(WHITE) << std::endl;
		std::cout << "           \\                                                     /" <<  std::endl;

		// Offset the wave strings around by 1 character on each frame by taking two substrings
		wave1 = wave1.substr(1, wave1.size() - 1) + wave1.substr(0, 1);
		wave2 = wave2.substr(1, wave2.size() - 1) + wave2.substr(0, 1);

		// Continue the hull of the ship behind the waves, but only if the space at the desired character is blank space
		if (wave1.at(12) == ' ')
		{ // Blank space, so we can replace it with the hull
			wave1.at(12) = '\\';
		}
		if (wave1.at(64) == ' ')
		{ // Blank space, so we can replace it with the hull
			wave1.at(64) = '/';
		}
		// Output the modified waves
		std::cout << setCol(TEAL) << wave1 << std::endl;
		std::cout << wave2 << setCol(WHITE) << std::endl;

		// Delete the added hull if they exist, so they don't stay there in the wave over multiple frames
		if (wave1.at(12) == '\\')
		{ // Hull is present, so delete it
			wave1.at(12) = ' ';
		}
		if (wave1.at(64) == '/')
		{ // Hull is present, so delete it
			wave1.at(64) = ' ';
		}

		// Edit the map for the current step
		std::string map2Temp = map2; // Create a temporary string copied from the full map string
		for (int i{f + 7}; i < 68; i++) // Go through the string, starting from an offset of the current frame + 7
		{ // Delete all points further along the path, replacing them with spaces
			map2Temp.at(i) = ' ';
		}
		// Output out the map boarder and modified strings
		// Top of the border
		std::cout << std::endl << "  " << setCol(TEAL) << box::TLd;
		for (int i{0}; i < 73; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::TRd << std::endl;
		// `Start/Finish' line and the modified `----' line
		std::cout << "  " << box::Vd << setCol(GREEN) << map1 << setCol(TEAL) << box::Vd << std::endl;
		std::cout << "  " << box::Vd << setCol(GREEN) << map2Temp << setCol(TEAL) << box::Vd << std::endl;
		// Bottom of the border
		std::cout << "  " << box::BLd;
		for (int i{0}; i < 73; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::BRd << setCol(WHITE);

		// Pause on each frame for 3/10ths of a second
		Sleep(300);
	}
}


// Display the truck-based cargo frame
void frameLand::dispFrame()
{
	// Create a new instance of the random event class, using smart pointers
	std::unique_ptr<event> excitement(new event());
	// `event' instance will be destroyed when `excitement' goes out of scope

	int nBox{0}; // Total number of boxes to be transported
	// Custom lambda function to sum up a vector
	std::for_each(invest::numLoaded.begin(), invest::numLoaded.end(), [&](int n) { nBox += n; }); // Add element n to the counter for_each element


	// Define strings for the road and map, both of which will be dynamic
	std::string road1 = "--  \\--\\  --   --   --  \\--\\  --   --   --  \\--\\  --   --   --  \\--\\  --   --  -";
	std::string road2 = " --  \\--\\  --   --   --  \\--\\  --   --   --  \\--\\  --   --   --  \\--\\  --   --  ";
	std::string map1 = "  Start                                                             End  ";
	std::string map2 = "    X -------------------------------------------------------------- X   ";

	// Start the main `movie' loop, consisting of 62 frames
	for (int f{0}; f < 62; f++)
	{
		if (f % 6 == 0)
		{ // Every 6 steps, generate and test for a random event via our event class
			excitement->genEvent();
		}

		system("CLS"); // Clear the screen

		// Test how many rows are required (either 1 or 2)
		int topRowTruck, botRowTruck;
		if (nBox > 11)
		{ // Too many containers for just one row
			botRowTruck = 11;
			topRowTruck = nBox - 11;
		}
		else
		{ // Only a single row
			botRowTruck = nBox;
			topRowTruck = 0;
		}

		// Pre-pad the top of the screen
		std::cout << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl;;

		// Start drawing the truck
		// The ASCII truck has been adapted from one found at: http://www.retrojunkie.com/asciiart/vehicles/trucks.htm
		std::cout << setCol(GRAY) << "               /|     " << box::TL << box::TR << std::endl;
		// Top of the top row
		std::cout << "               ||     " << box::V << box::V;
		if (topRowTruck > 0)
		{ // There are more than 11 containers, so start drawing the top row
			std::cout << box::TL;
			for (int i{0}; i < topRowTruck; i++)
			{
				std::cout << box::H << box::H << box::H;
				if (i == topRowTruck - 1)
				{ // Final box in the row, finish off the corner
					std::cout << box::TR;
				}
				else
				{ // More boxes to go, so join them up!
					std::cout << box::T;
				}
			}
		}
		std::cout << std::endl;

		// Middle of top row
		std::cout << setCol(RED) << "          .----" << setCol(GRAY) << "|" << setCol(RED) << "-----." << setCol(GRAY) << box::V << box::V;
		if (topRowTruck > 0)
		{
			std::cout << box::V;
			for (int i{0}; i < topRowTruck; i++)
			{
				std::cout << "   " << box::V;
			}
		}
		std::cout << std::endl;

		// Top of the bottom row, but correct for any empty space above
		std::cout << setCol(RED) << "          || " << box::smile << "|" << setCol(GRAY) << "|" << setCol(RED) << "   ==|" << setCol(GRAY) << box::V << box::V;
		if (topRowTruck == 0)
		{ // No need to connect upwards, just print box::T pieces
			std::cout << box::TL;
			for (int i{0}; i < botRowTruck; i++)
			{
				std::cout << box::H << box::H << box::H;
				if (i + 1 == botRowTruck)
				{
					std::cout << box::TR;
				}
				else
				{
					std::cout << box::T;
				}
			}
			std::cout << std::endl;
		}
		else
		{ // Need to worry about connecting upwards
			std::cout << box::R;
			for (int i{0}; i < topRowTruck; i++)
			{
				if (i == 10)
				{
					break;
				}
				std::cout << box::H << box::H << box::H << box::P;
			}
			for (int i{0}; i < 10 - topRowTruck; i++)
			{
				std::cout << box::H << box::H << box::H << box::T;
			}
			if (topRowTruck == 11)
			{ // Then the row above is completely full, need to have the top left corner connect
				std::cout << box::H << box::H << box::H << box::L << std::endl;
			}
			else
			{ // The top right corner is empty, so round off the edge
				std::cout << box::H << box::H << box::H << box::TR << std::endl;
			}
		}

		// Middle of the bottom row
		std::cout << setCol(RED) << "     .-----'--'" << setCol(GRAY) << "|" << setCol(RED) << "   ==|" << setCol(GRAY) << box::V << box::V;
		std::cout << box::V;
		for (int i{0}; i < botRowTruck; i++)
		{
			std::cout << "   " << box::V;
		}
		std::cout << std::endl;

		// Bottom of the bottom row
		std::cout << setCol(RED) << "     " << setCol(YELLOW) << "|" << setCol(RED) << ")-      ~" << setCol(GRAY) << "|" << setCol(RED) << "     |" << setCol(GRAY) << box::BL << box::Tu << box::Tu;
		for (int i{0}; i < botRowTruck; i++)
		{ // Connect the base to the boxes
			std::cout << box::H << box::H << box::H << box::Tu;
		}
		for (int i{0}; i < 11 - botRowTruck; i++)
		{ // No more boxes, so fill out the rest of the base
			std::cout << box::H << box::H << box::H << box::H;
		}
		std::cout << box::H << box::H << box::H << box::H << box::TR << std::endl; // Finish off the base
		// Output the rest of the truck
		std::cout << setCol(RED) << "     | ___     " << setCol(GRAY) << "|" << setCol(RED) << "     |____...==..._  >\\______________________________| " << setCol(GRAY) << box::BL << box::H << box::BR << std::endl;
		std::cout << setCol(RED) << "    [_/" << setCol(GRAY) << ".-." << setCol(RED) << "\\\"" << setCol(GRAY) << "--" << setCol(RED) << "\"-------- //" << setCol(GRAY) << ".-.  .-." << setCol(RED) << "\\\\/   |/            \\\\ " << setCol(GRAY) << ".-.  .-." << setCol(RED) << " //" << setCol(GRAY) << std::endl;
		std::cout << "      ( o )" << setCol(RED) << "`===\"\"\"\"\"\"\"\"\"`" << setCol(GRAY) << "( o )( o )" << setCol(RED) << "     o              `" << setCol(GRAY) << "( o )( o )" << setCol(RED) << "`" << setCol(GRAY) << std::endl;

		// Offset the road strings around by 1 character on each frame by taking two substrings
		road1 = road1.substr(road1.size() - 2, road1.size() - 1) + road1.substr(0, road1.size() - 2);
		road2 = road2.substr(road2.size() - 2, road2.size() - 1) + road2.substr(0, road2.size() - 2);

		// Replace characters where the base of the tires have to go
		std::vector<char> replaced = {road1.at(7), road1.at(8), road1.at(9), road1.at(26), road1.at(27), road1.at(28), road1.at(31), road1.at(32), road1.at(33), road1.at(57), road1.at(58), road1.at(59), road1.at(62), road1.at(63), road1.at(64)};
		road1.at(7) = '\'';
		road1.at(8) = '-';
		road1.at(9) = '\'';
		road1.at(26) = '\'';
		road1.at(27) = '-';
		road1.at(28) = '\'';
		road1.at(31) = '\'';
		road1.at(32) = '-';
		road1.at(33) = '\'';
		road1.at(57) = '\'';
		road1.at(58) = '-';
		road1.at(59) = '\'';
		road1.at(62) = '\'';
		road1.at(63) = '-';
		road1.at(64) = '\'';

		// Output the road strings
		std::cout << road1;
		std::cout << road2 << std::endl;

		// Reset the road characters where the tyres were
		road1.at(7) = replaced.at(0);
		road1.at(8) = replaced.at(1);
		road1.at(9) = replaced.at(2);
		road1.at(26) = replaced.at(3);
		road1.at(27) = replaced.at(4);
		road1.at(28) = replaced.at(5);
		road1.at(31) = replaced.at(6);
		road1.at(32) = replaced.at(7);
		road1.at(33) = replaced.at(8);
		road1.at(57) = replaced.at(9);
		road1.at(58) = replaced.at(10);
		road1.at(59) = replaced.at(11);
		road1.at(62) = replaced.at(12);
		road1.at(63) = replaced.at(13);
		road1.at(64) = replaced.at(14);

		// Pre-pad for the random event message
		for (int i{0}; i < static_cast<int>((78 - excitement->getExampleEvent().second.size()) / 2); i++)
		{
			std::cout << " ";
		}
		// Alter the message's colour based on if the event was positive (green) or negative (red)
		if (excitement->getExampleEvent().first)
		{ // Positive event
			std::cout << setCol(GREEN);
		}
		else
		{ // Negative event
			std::cout << setCol(RED);
		}
		// Display the actual message, accessed from our events class
		std::cout << excitement->getExampleEvent().second << setCol(WHITE) << std::endl << std::endl;

		// Edit the map for the current step
		std::string map2Temp = map2; // Create a temporary string copied from the full map string
		for (int i{f + 7}; i < 68; i++) // Go through the string, starting from an offset of the current frame + 7
		{ // Delete all points further along the path, replacing them with spaces
			map2Temp.at(i) = ' ';
		}
		// Output out the map boarder and modified strings
		// Top of the border
		std::cout << "  " << setCol(TEAL) << box::TLd;
		for (int i{0}; i < 73; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::TRd << std::endl;
		// `Start/Finish' line and the modified `----' line
		std::cout << "  " << box::Vd << setCol(GREEN) << map1 << setCol(TEAL) << box::Vd << std::endl;
		std::cout << "  " << box::Vd << setCol(GREEN) << map2Temp << setCol(TEAL) << box::Vd << std::endl;
		// Bottom of the border
		std::cout << "  " << box::BLd;
		for (int i{0}; i < 73; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::BRd;

		// Pause on each frame for 3/10ths of a second
		Sleep(300);
	}
}


// Display the plane-based cargo frame
void frameAir::dispFrame()
{
	// Create a new instance of the random event class, using smart pointers
	std::unique_ptr<event> excitement(new event());
	// `event' instance will be destroyed when `excitement' goes out of scope

	int nBox{0}; // Total number of boxes to be transported
	// Custom lambda function to sum up a vector
	std::for_each(invest::numLoaded.begin(), invest::numLoaded.end(), [&](int n) { nBox += n; }); // Add element n to the counter for_each element


	// Define strings for the clouds and map, both of which will be dynamic
	// The ASCII clouds have been adapted from the ones found at: http://www.retrojunkie.com/asciiart/nature/clouds.htm
	std::string cloud1 = "                                                                       _       ";
	std::string cloud2 = "                               _ .                                    (  )     ";
	std::string cloud3 = "                             (  _ )_                               ( `  ) . )  ";
	std::string cloud4 = "                            (_  _(_, )                            (_, _(  ,_)_)";
	std::string map1 = "  Start                                                             End  ";
	std::string map2 = "    X -------------------------------------------------------------- X   ";

	// Start the main `movie' loop, consisting of 62 frames
	for (int f{0}; f < 62; f++)
	{
		if (f % 8 == 0)
		{ // Every 8 steps, generate and test for a random event via our event class
			excitement->genEvent();
		}

		// Offset the cloud strings around by 1 character on each frame by taking two substrings
		cloud1 = cloud1.substr(1, cloud1.size() - 1) + cloud1.substr(0, 1);
		cloud2 = cloud2.substr(1, cloud2.size() - 1) + cloud2.substr(0, 1);
		cloud3 = cloud3.substr(1, cloud3.size() - 1) + cloud3.substr(0, 1);
		cloud4 = cloud4.substr(1, cloud4.size() - 1) + cloud4.substr(0, 1);

		system("CLS"); // Clear the screen

		// Start drawing the frame
		std::cout << std::endl; // Line padding
		// Output the cloud strings
		std::cout << cloud1 << std::endl;
		std::cout << cloud2 << std::endl;
		std::cout << cloud3 << std::endl;
		std::cout << cloud4 << std::endl << std::endl << std::endl;

		// Start drawing the plane
		// The ASCII plane has been adapted from the one found at: http://www.ascii-art.de/ascii/pqr/plane.txt
		std::cout << setCol(GRAY) << "             -.                    `|" << setCol(RED) << "." << setCol(GRAY) << std::endl;
		std::cout << "             |" << setCol(YELLOW) << ":" << setCol(GRAY) << "\\-,                 .| \\." << std::endl;
		std::cout << "             |" << setCol(YELLOW) << ":" << setCol(GRAY) << " `---------------------------------------------." << std::endl;
		// Start drawing the boxes
		// Top of the only row, lucky us!
		std::cout << "             / /  " << setCol(WHITE) << box::TL; // The back wing of the plane
		for (int i{0}; i < nBox; i++)
		{ // Iterate through the number of boxes
			std::cout << box::H << box::H << box::H;
			if (i + 1 == nBox)
			{ // The final box, so finish it
				std::cout << box::TR;
			}
			else
			{ // Carry on connecting to the next box
				std::cout << box::T;
			}
		}
		for (int i{0}; i < 10 - nBox; i++)
		{ // Pad for the rest of the empty plane
			std::cout << "    ";
		}
		std::cout << setCol(GRAY) << "  (_`." << std::endl; // Cockpit of the plane

		// Middle of the only row
		std::cout << "            /_ \\  " << setCol(WHITE) << box::V;
		for (int i{0}; i < nBox; i++)
		{ // Draw |   | for the number of boxes
			std::cout << "   " << box::V;
		}
		for (int i{0}; i < 10 - nBox; i++)
		{ // Pad for the rest of the empty space
			std::cout << "    ";
		}
		std::cout << setCol(GRAY) << "     `)" << std::endl; // The nose of the plane

		// Bottom of the only row
		std::cout << "                \\ " << setCol(WHITE) << box::BL; // Tail of the plane
		for (int i{0}; i < nBox; i++)
		{ // Draw the floor of the boxes
			std::cout << box::H << box::H << box::H;
			if (i + 1 == nBox)
			{ // Final box, round out the corner
				std::cout << box::BR;
			}
			else
			{ // Keep connecting to the next box
				std::cout << box::Tu;
			}
		}
		for (int i{0}; i < 10 - nBox; i++)
		{ // Pad the rest of the space
			std::cout << "    ";
		}

		// Finish drawing the plane
		std::cout << setCol(GRAY) << "     /" << std::endl;
		std::cout << "                 `---------------/" << setCol(YELLOW) << "/" << setCol(GRAY) << "    /-----------------------'" << std::endl;
		std::cout << "                               </" << setCol(YELLOW) << "/" << setCol(GRAY) << "   /_(" << setCol(DGRAY) << "+" << setCol(GRAY) << ")" << std::endl;
		std::cout << "                               /" << setCol(YELLOW) << "/" << setCol(GRAY) << "  /" << std::endl;
		std::cout << "                              /" << setCol(YELLOW) << "/" << setCol(GRAY) << " /" << std::endl;
		std::cout << "                            ----" << setCol(GREEN) << "'" << setCol(GRAY) << std::endl;

		// Calculate the amount of pre-padding required for the random message
		for (int i{0}; i < static_cast<int>((78 - excitement->getExampleEvent().second.size()) / 2); i++)
		{
			std::cout << " ";
		}
		// Alter the message's colour based on if the event was positive (green) or negative (red)
		if (excitement->getExampleEvent().first)
		{ // Positive event
			std::cout << setCol(GREEN);
		}
		else
		{ // Negative event
			std::cout << setCol(RED);
		}
		// Display the actual message, accessed from our events class
		std::cout << excitement->getExampleEvent().second << setCol(GRAY) << std::endl;

		// Edit the map for the current step
		std::string map2Temp = map2; // Create a temporary string copied from the full map string
		for (int i{f + 7}; i < 68; i++) // Go through the string, starting from an offset of the current frame + 7
		{ // Delete all points further along the path, replacing them with spaces
			map2Temp.at(i) = ' ';
		}
		// Output out the map boarder and modified strings
		// Top of the border
		std::cout << std::endl << "  " << setCol(TEAL) << box::TLd;
		for (int i{0}; i < 73; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::TRd << std::endl;
		// `Start/Finish' line and the modified `----' line
		std::cout << "  " << box::Vd << setCol(GREEN) << map1 << setCol(TEAL) << box::Vd << std::endl;
		std::cout << "  " << box::Vd << setCol(GREEN) << map2Temp << setCol(TEAL) << box::Vd << std::endl;
		// Bottom of the border
		std::cout << "  " << box::BLd;
		for (int i{0}; i < 73; i++)
		{
			std::cout << box::Hd;
		}
		std::cout << box::BRd << setCol(WHITE);

		// Pause on each frame for 3/10ths of a second
		Sleep(300);
	}
}


// Memeber function to change the current container selected in the list
void receipt::loopCargo(const int increment)
{
	currentSel += increment; // Increment

	// Validate the new value
	if (currentSel < 0)
	{ // At container index == -1, so loop it back to the max
		currentSel = invest::cargoList.size() - 1;
	}
	else if (static_cast<size_t>(currentSel) > invest::cargoList.size() - 1)
	{ // At container index == cargoList.size(), so loop it back to 0
		currentSel = 0;
	}
}

// Member function to display the cargo receipt screen
void receipt::dispFrame()
{
	/* After a trip, display the list of containers loaded and allow the user to select each one to display it's information
	 Also display, in table, the ID, investment cost, investment return (can be + or - the investment cost)
	 Then at the bottom, sum each of the columns and work out the net money from trip
	 */

	// Define vectors for the total money spent and earnt from each container
	std::vector<int> spentContainer; // Loss
	std::vector<int> earntContainer; // Gain
	std::vector<int> netContainer; // Net (Gain - Loss)
	// Declare and initialise summations for the above vectors
	int spent{0};
	int earnt{0};
	int net{0};

	// Run until a valid input is entered
	bool loopReceipt{true};
	while (loopReceipt)
	{
		// Reset the money totals every time, otherwise they will keep being summed on every key press
		spentContainer.clear();
		earntContainer.clear();
		netContainer.clear();
		spent = 0;
		earnt = 0;
		net = 0;

		for (auto iter = invest::cargoList.begin(); iter < invest::cargoList.end(); ++iter)
		{ // Iterate through the cargo list to oush-back the amount of money earnt and spent
			spentContainer.push_back((*iter)->getCost());
			earntContainer.push_back((*iter)->calcValue());
		}
		// Work out the total net gain in money for each container, using our templated overloaded operator- function
		netContainer = earntContainer - spentContainer;
		// Sum each of the vectors using custom lambda functions
		std::for_each(spentContainer.begin(), spentContainer.end(), [&](int n)	{ spent += n; });
		std::for_each(earntContainer.begin(), earntContainer.end(), [&](int n)	{ earnt += n; });
		std::for_each(netContainer.begin(), netContainer.end(), [&](int n)	{ net += n; });


		system("CLS"); // Clear the screen

		// Output the summary and the currently selected container's properties
		std::cout << std::endl;
		std::cout << setCol(WHITE) << "  Here is a summary of the delivered cargo:    " << setCol(RED) << box::TLd << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::TRd << setCol(WHITE) << "    Cost: " << setCol(RED) << box::GBP << invest::cargoList.at(currentSel)->getCost() << setCol(WHITE) << std::endl;
		std::cout << "   Use " << setCol(GRAY) << box::arrU << setCol(WHITE) << " and " << setCol(GRAY) << box::arrD << setCol(WHITE) << " to navigate cargo list          " << setCol(RED) << box::V << setCol(WHITE) << " ID: " << setCol(TEAL) << invest::cargoList.at(currentSel)->getID() << " " << setCol(RED) << box::V << setCol(WHITE) << "  Return: " << setCol(GREEN) << box::GBP << invest::cargoList.at(currentSel)->calcValue() << setCol(WHITE) << std::endl;
		std::cout << "   #     ID        " << setCol(RED) << "Cost" << setCol(GREEN) << "       Return\t       " << setCol(RED) << box::BLd << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::H << box::BRd << setCol(WHITE) << "     Net: " << setCol(YELLOW) << box::GBP << netContainer.at(currentSel) << setCol(WHITE) <<  std::endl;
		// Start the container list table
		if (invest::cargoList.size() > 15 && currentSel > 0)
		{ // There are more containers above the top of the table, so display 3 arrows to indicate this
			std::cout << setCol(GRAY) << "  --  ------- " << setCol(PINK) << box::arrU << box::arrU << box::arrU << setCol(WHITE) << " ------     -------    ";
		}
		else
		{ // We are at the top of the list, so no arrows
			std::cout << setCol(GRAY) << "  --  -------     ------     -------    " << setCol(WHITE);
		}
		// Continuation of the selected container's information
		std::cout << "  Width: " << setCol(TEAL) << std::to_string(invest::cargoList.at(currentSel)->getWidth()).substr(0, 4) << setCol(WHITE) << " m     Height: " << setCol(TEAL) << std::to_string(invest::cargoList.at(currentSel)->getHeight()).substr(0, 4) << setCol(WHITE) << " m" << std::endl;

		// Loop through the first 15 containers after the current selection
		for (int i{currentSel}; i < currentSel + 15; i++)
		{
			if (static_cast<size_t>(i) > invest::cargoList.size() - 1)
			{ // No more containers left in the list, so break from the for loop
				break;
			}
			if (i == currentSel)
			{ // Mark which box is currently selected for display by inverting its colours
				std::cout << " " << setCol(BLACK, WHITE);
			}
			else
			{ // Display with normal colours
				std::cout << " " << setCol(WHITE);
			}

			// Display the actual container in the row
			std::cout << " " << std::setw(2) << std::right << i + 1 << std::setw(9) << std::right << invest::cargoList.at(i)->getID() << std::setw(6) << std::right << box::GBP << " " << std::setw(4) << std::left << invest::cargoList.at(i)->getCost() << std::setw(6) << std::right << box::GBP << " " << std::setw(5) << std::left << invest::cargoList.at(i)->calcValue() << setCol(WHITE);

			// At the first row in the table, we need to finish off the current container's properties
			if (i == currentSel)
			{
				std::cout << "     Length: " << setCol(TEAL) << std::to_string(invest::cargoList.at(currentSel)->getLength()).substr(0, 4) << setCol(WHITE) << " m  Max. Load: " << setCol(TEAL) << std::to_string(invest::cargoList.at(currentSel)->getMaxLoad()) << setCol(WHITE) << " kg";
			}
			std::cout << std::endl;
		}

		if (invest::cargoList.size() > static_cast<size_t>(15) && static_cast<size_t>(currentSel + 15) < invest::cargoList.size())
		{ // There are more containers below the bottom of the table, so display 3 arrows to indicate this
			std::cout << "              " << setCol(PINK) << box::arrD << box::arrD << box::arrD << setCol(WHITE) << std::endl;
		}
		else
		{ // We are within the last 15 containers on the list, so no arrows - but we want to pad with extra lines to keep the table base static
			for (size_t i{ 0 }; i < static_cast<size_t>(currentSel)-invest::cargoList.size() + 16; i++)
			{ // An extra line for every missing container
				std::cout << std::endl;
			}
		}

		// Display the base of the container, including how much the user spent and earnt on their latest trip
		std::cout << setCol(GRAY) << "  -------------------------------------" << setCol(WHITE) << std::endl;
		std::cout << "   Spent: " << setCol(RED) << box::GBP << " " << std::setw(6) << std::left << spent << setCol(WHITE) << std::endl;
		std::cout << "   Earnt: " << setCol(GREEN) << box::GBP << " " << std::setw(6) << std::left << earnt << setCol(WHITE);
		std::cout << "/ Total Net: " << setCol(YELLOW) << box::GBP << " " << net << setCol(WHITE) << "\t    Press " << setCol(GRAY) << "Enter" << setCol(WHITE) << " or " << setCol(GRAY) << "Esc" << setCol(WHITE) << " to continue..." << std::endl;


		bool invalidKey{ true };
		while (invalidKey)
		{ // Loop until we get a valid key input, otherwise the screen will refresh on every input!
			INPUT_RECORD irInput = getKeyPress(); // Get any user input
			if (irInput.Event.KeyEvent.bKeyDown)
			{
				switch (irInput.Event.KeyEvent.wVirtualKeyCode)
				{
				case VK_ESCAPE: // Finish with the receipt screen
					invalidKey = false; // Break the key validation loop
					loopReceipt = false;
					break;
				case VK_RETURN: // Finish with the receipt screen
					invalidKey = false; // Break the key validation loop
					loopReceipt = false;
					break;
				case VK_UP: // Move up the container list
					invalidKey = false; // Break the key validation loop
					loopCargo(-1);
					break;
				case VK_DOWN: // Move down the container list
					invalidKey = false; // Break the key validation loop
					loopCargo(1);
					break;
				}
			}
		}
	}

	// Before we leave, update the money to account for how much the user made on this trip
	invest::money += earnt;
}
