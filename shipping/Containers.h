#ifndef CONTAINERS_H
#define CONTAINERS_H
// ^ Header guard

// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include <iomanip>
#include "Support.h"


// Abstract base class for containers
class container
{
protected:
	// Standard dimensional/physical property variables
	double width = 0; // Width of container [m]
	double height = 0; // Height of container [m]
	double length = 0; // Length of container [m]
	int maxLoad = 0; // Maximum weight of container [kg]

	// Monetary related variables
	int cost = 0; // Cost of container in [GBP], will not be modified once set
	double valueMod = 0; // A variable that modifies the container's worth (a multiplier for the base cost)

	// Identification variables
	int medium = 0; // Identifier for which medium the transport is in [0,1,2 = sea,land,air]
	int type = 0; // Identifier for where the container is used [0,1,2,3,4]
	std::string id; // Unique identifier string

	virtual double calcVol() const final
	{ // Return the volume of the container
		return height * length * width;
	}

public:
	virtual ~container(){} // Virtual destructor

	// Gets
	virtual double getWidth() final // Return the container's width
	{ return width; }

	virtual double getHeight() final // Return the container's height
	{ return height; }

	virtual double getLength() final // Return the container's length
	{ return length; }

	virtual int getMaxLoad() final // Return the container's maximum load
	{ return maxLoad; }

	virtual int getCost() final // Return the container's cost
	{ return cost; }

	virtual double getValueMod() final // Return the container's value modifier
	{ return valueMod; }

	virtual int getMedium() = 0; // Pure virtual function to return the medium of transport (0 = sea / 1 = land / 2 = air)
	
	virtual int getType() final // Return the container's type
	{ return type; }

	virtual std::string getID() final // Return the container's ID
	{ return id; }


	// Sets
	virtual void setWidth(double val) final // Set the container's width
	{ width = val; }
	
	virtual void setHeight(double val) final // Set the container's height
	{ height = val; }
	
	virtual void setLength(double val) final // Set the container's length
	{ length = val; }
	
	virtual void setMaxLoad(int val) final // Set the container's maxload
	{ maxLoad = val; }
	
	virtual void setCost(int val) final // Set the container's cost
	{ cost = val; }
	
	virtual void setValueMod(double val) final // Set the container's base value modifier
	{ valueMod = val; }

	virtual void setID(std::string val) final // Set the container's id
	{ id = val; }
	
	virtual void setValue(const double mod) final // Modify the container's monetary value
	{ valueMod += mod; }


	// Member functions
	virtual int calcValue() const final
	{ // Return the container's value, round the base value by valueMod to get the actual value
		return static_cast<int>(round(cost * valueMod));
	}

	friend std::ostream& operator<<(std::ostream& os, const container& cont); // Friend function for the overloading of the output stream operator<<
	// See this page on the Virtual Friend Function Idiom: https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Virtual_Friend_Function
	
	virtual void dispInfo(std::ostream& os) const = 0; // Pure virtual function to alter the ostream with the derived container's information
	virtual void dispMaths(std::ostream& os) const; // Pure virtual function to disply a container only if it has been added/multiplied within the `Reference' menu

	// Virtual overloading of the addition operator to add together two containers
	virtual container& operator+(container& cont) const = 0;
	// Virtual overloading of the multiplication operator to scale a containers' properties
	virtual container& operator*(const int& num) const = 0;

};

// Define a public access function for overloading the output operator
inline std::ostream& operator<<(std::ostream& os, const container& cont)
{ // This is the only way I could find to have this in the base class, rather than repeating it into each 2nd gen derived classes
	
	cont.dispInfo(os); // Call the dispInfo function of the reference container

	return os; // Return the output stream
}

// A public access function to display the container's information if it has been involved in addition/multiplication within the `Reference' page. It is implemented here so we don't have to make it 'virtual' and copy the exact same thing into each function
inline void container::dispMaths(std::ostream& os) const
{
	// Display the different properties of the container, with formatting
	os << setCol(WHITE) << "  Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg" << setCol(WHITE) << ", Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m" << setCol(WHITE) << ", Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m" << setCol(WHITE) << ", Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m" << setCol(WHITE) << "," << std::endl;
	os << setCol(WHITE) << "  Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3" << setCol(WHITE) << ", Cost: " << setCol(YELLOW) << box::GBP << " " << cost << setCol(WHITE) << ", Base Value: " << setCol(YELLOW) << box::GBP << calcValue() << setCol(WHITE);
}


// Abstract derived class for ship-based containers in water
class containerSea : public container
{
public:
	virtual ~containerSea() = default; // Virtual destructor

	int getMedium() override final
	{ // Override the medium return, must be of medium 'sea' = 0
		return 0;
	}
};

// Abstract derived class for truck-based containers on land
class containerLand : public container
{
public:
	virtual ~containerLand() = default; // Virtual destructor

	int getMedium() override final
	{ // Override the medium return, must be of medium 'land' = 1
		return 1;
	}
};

// Abstract derived class for plane-based containers in air
class containerAir : public container
{
public:
	virtual ~containerAir() = default; // Virtual destructor

	int getMedium() override final
	{ // Override the medium return, must be of medium 'air' = 2
		return 2;
	}
};


// Place each medium's container in its own namespace, so the same container type name can be used
/* Sea containers */
namespace sea
{
	// Dervied class for ship-based normal containers
	class normal : public containerSea
	{
		friend std::ostream& operator<<(std::ostream& os, const normal& cont); // Friend function to overload the output operator and display the container's information

	public:
		normal(); // Default constructor
		normal(std::string idCustom) : normal() { id = "NRM" + idCustom; } // Paramaterised constructor, only alters the container's ID
		// Paramaterised constructor uses C++11 Delegating Constructors, see: https://msdn.microsoft.com/en-gb/library/dn387583.aspx#Anchor_2

		~normal() {} // Destructor 

		// Overloading operators
		normal& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Dervied class for ship-based flat rack containers
	class flat : public containerSea
	{
		friend std::ostream& operator<<(std::ostream& os, const flat& cont); // Friend function to overload the output operator and display the container's information

	public:
		flat(); // Default constructor
		flat(std::string idCustom) : flat() { id = "FLT" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~flat() {} // Destructor

		// Overloading operators
		flat& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Derived class for ship-based refrigerated containers
	class fridge : public containerSea
	{
		friend std::ostream& operator<<(std::ostream& os, const fridge& cont); // Friend function to overload the output operator and display the container's information

	public:
		fridge(); // Default constructor
		fridge(std::string idCustom) : fridge() { id = "FRG" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~fridge() {} // Destructor

		// Overloading operators
		fridge& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Derived class for ship-based liquid containers
	class liquid : public containerSea
	{
		friend std::ostream& operator<<(std::ostream& os, const liquid& cont); // Friend function to overload the output operator and display the container's information

	public:
		liquid(); // Default constructor
		liquid(std::string idCustom) : liquid() { id = "LQD" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~liquid() {} // Destructor

		// Overloading operators
		liquid& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Derived class for ship-based hazardous material containers
	class hazard : public containerSea
	{
		friend std::ostream& operator<<(std::ostream& os, const hazard& cont); // Friend function to overload the output operator and display the container's information

	public:
		hazard(); // Default constructor
		hazard(std::string idCustom) : hazard() { id = "HZD" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~hazard() {} // Destructor

		// Overloading operators
		hazard& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};
}

/* Land containers */
namespace land
{
	// Dervied class for truck-based normal containers
	class normal : public containerLand
	{
		friend std::ostream& operator<<(std::ostream& os, const normal& cont); // Friend function to overload the output operator and display the container's information

	public:
		normal(); // Default constructor
		normal(std::string idCustom) : normal() { id = "NRM" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~normal() {} // Destructor 

		// Overloading operators
		normal& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Dervied class for truck-based flat rack containers
	class flat : public containerLand
	{
		friend std::ostream& operator<<(std::ostream& os, const flat& cont); // Friend function to overload the output operator and display the container's information

	public:
		flat(); // Default constructor
		flat(std::string idCustom) : flat() { id = "FLT" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~flat() {} // Destructor

		// Overloading operators
		flat& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Derived class for truck-based refrigerated containers
	class fridge : public containerLand
	{
		friend std::ostream& operator<<(std::ostream& os, const fridge& cont); // Friend function to overload the output operator and display the container's information

	public:
		fridge(); // Default constructor
		fridge(std::string idCustom) : fridge() { id = "FRG" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~fridge() {} // Destructor

		// Overloading operators
		fridge& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Derived class for truck-based liquid containers
	class liquid : public containerLand
	{
		friend std::ostream& operator<<(std::ostream& os, const liquid& cont); // Friend function to overload the output operator and display the container's information

	public:
		liquid(); // Default constructor
		liquid(std::string idCustom) : liquid() { id = "LQD" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~liquid() {} // Destructor

		// Overloading operators
		liquid& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Derived class for truck-based hazardous material containers
	class hazard : public containerLand
	{
		friend std::ostream& operator<<(std::ostream& os, const hazard& cont); // Friend function to overload the output operator and display the container's information

	public:
		hazard(); // Default constructor
		hazard(std::string idCustom) : hazard() { id = "HZD" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~hazard() {} // Destructor

		// Overloading operators
		hazard& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};
}

/* Air containers */
namespace air
{
	// Dervied class for plane-based normal containers
	class normal : public containerAir
	{
		friend std::ostream& operator<<(std::ostream& os, const normal& cont); // Friend function to overload the output operator and display the container's information

	public:
		normal(); // Default constructor
		normal(std::string idCustom) : normal() { id = "NRM" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~normal() {} // Destructor 

		// Overloading operators
		normal& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Dervied class for plane-based flat rack containers
	class flat : public containerAir
	{
		friend std::ostream& operator<<(std::ostream& os, const flat& cont); // Friend function to overload the output operator and display the container's information

	public:
		flat(); // Default constructor
		flat(std::string idCustom) : flat() { id = "FLT" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~flat() {} // Destructor

		// Overloading operators
		flat& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Derived class for plane-based refrigerated containers
	class fridge : public containerAir
	{
		friend std::ostream& operator<<(std::ostream& os, const fridge& cont); // Friend function to overload the output operator and display the container's information

	public:
		fridge(); // Default constructor
		fridge(std::string idCustom) : fridge() { id = "FRG" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~fridge() {} // Destructor

		// Overloading operators
		fridge& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Derived class for plane-based liquid containers
	class liquid : public containerAir
	{
		friend std::ostream& operator<<(std::ostream& os, const liquid& cont); // Friend function to overload the output operator and display the container's information

	public:
		liquid(); // Default constructor
		liquid(std::string idCustom) : liquid() { id = "LQD" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~liquid() {} // Destructor

		// Overloading operators
		liquid& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};

	// Derived class for plane-based hazardous material containers
	class hazard : public containerAir
	{
		friend std::ostream& operator<<(std::ostream& os, const hazard& cont); // Friend function to overload the output operator and display the container's information

	public:
		hazard(); // Default constructor
		hazard(std::string idCustom) : hazard() { id = "HZD" + idCustom; } // Paramaterised constructor, only alters the container's ID

		~hazard() {} // Destructor

		// Overloading operators
		hazard& operator=(container& cont); // Copy assignment operator for deep copying
		container& operator+(container& cont) const override; // Addition of containers
		container& operator*(const int& num) const override; // Scalar multiplication of a container

		// Member functions
		void dispInfo(std::ostream& os) const override; // Overriden function to modify the ostream with the current container's information
	};
}

// End of header guard
#endif // CONTAINERS_H
