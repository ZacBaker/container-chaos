// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include "Containers.h"

#include <memory>


/*
	SHIP CONTAINERS 
					*/

// Default constructor for ship-based normal containers
sea::normal::normal()
{
	width = 2.34;
	height = 2.29;
	length = 5.90;
	maxLoad = 21727;

	cost = 100;
	valueMod = 1.1;

	type = 0;
	id = std::string("NRM?");
}

// Copy assignment operator for deep copying of ship-based normal containers
sea::normal& sea::normal::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& sea::normal::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator. WARNING: Can't use smart pointers, else they are deleted after this function exits!
	normal* addedCont(new normal());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& sea::normal::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	normal* multipliedCont(new normal());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on ship-based normal containers
void sea::normal::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 46; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Ship" << setCol(WHITE) << "-based " << setCol(TEAL) << "normal" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 46; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for ship-based flat-bed containers
sea::flat::flat()
{
	width = 2.44;
	height = 5.00;
	length = 5.64;
	maxLoad = 42100;

	cost = 200;
	valueMod = 1.2;

	type = 1;
	id = std::string("FLT?");
}

// Copy assignment operator for deep copying of ship-based normal containers
sea::flat& sea::flat::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& sea::flat::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	flat* addedCont(new flat());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& sea::flat::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	flat* multipliedCont(new flat());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on ship-based flat-bed containers
void sea::flat::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 48; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Ship" << setCol(WHITE) << "-based " << setCol(TEAL) << "flat-bed" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                       " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                     " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                     " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 48; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for ship-based refrigerated containers
sea::fridge::fridge()
{
	width = 2.28;
	height = 2.33;
	length = 2.45;
	maxLoad = 27320;

	cost = 300;
	valueMod = 1.3;

	type = 2;
	id = std::string("FRG?");
}

// Copy assignment operator for deep copying of ship-based normal containers
sea::fridge& sea::fridge::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& sea::fridge::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	fridge* addedCont(new fridge());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& sea::fridge::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	fridge* multipliedCont(new fridge());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on ship-based refrigerated containers
void sea::fridge::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 46; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Ship" << setCol(WHITE) << "-based " << setCol(TEAL) << "fridge" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 46; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for ship-based liquid containers
sea::liquid::liquid()
{
	width = 2.43;
	height = 2.59;
	length = 6.09;
	maxLoad = 26001;

	cost = 400;
	valueMod = 1.4;

	type = 3;
	id = std::string("LQD?");
}

// Copy assignment operator for deep copying of ship-based normal containers
sea::liquid& sea::liquid::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& sea::liquid::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	liquid* addedCont(new liquid());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& sea::liquid::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	liquid* multipliedCont(new liquid());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on ship-based liquid containers
void sea::liquid::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 46; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Ship" << setCol(WHITE) << "-based " << setCol(TEAL) << "liquid" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 46; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for ship-based hazardous containers
sea::hazard::hazard()
{
	width = 2.30;
	height = 2.40;
	length = 6.00;
	maxLoad = 13000;

	cost = 800;
	valueMod = 1.8;

	type = 4;
	id = std::string("HZD?");
}

// Copy assignment operator for deep copying of ship-based normal containers
sea::hazard& sea::hazard::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& sea::hazard::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	hazard* addedCont(new hazard());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& sea::hazard::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	hazard* multipliedCont(new hazard());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on ship-based hazardous containers
void sea::hazard::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 46; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Ship" << setCol(WHITE) << "-based " << setCol(TEAL) << "hazard" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                     " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 46; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


/*
	TRUCK CONTAINERS
					*/

// Default constructor for truck-based normal containers
land::normal::normal()
{
	width = 2.11;
	height = 2.06;
	length = 5.31;
	maxLoad = 19554;

	cost = 100;
	valueMod = 1.1;

	type = 0;
	id = std::string("NRM?");
}

// Copy assignment operator for deep copying of ship-based normal containers
land::normal& land::normal::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& land::normal::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	normal* addedCont(new normal());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& land::normal::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	normal* multipliedCont(new normal());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on truck-based normal containers
void land::normal::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Truck" << setCol(WHITE) << "-based " << setCol(TEAL) << "normal" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for truck-based flat-bed containers
land::flat::flat()
{
	width = 2.20;
	height = 4.50;
	length = 5.08;
	maxLoad = 37890;

	cost = 200;
	valueMod = 1.2;

	type = 1;
	id = std::string("FLT?");
}

// Copy assignment operator for deep copying of ship-based normal containers
land::flat& land::flat::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& land::flat::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	flat* addedCont(new flat());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& land::flat::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	flat* multipliedCont(new flat());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on truck-based flat-bed containers
void land::flat::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 49; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Truck" << setCol(WHITE) << "-based " << setCol(TEAL) << "flat-bed" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                      " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                      " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                     " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                      " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                      " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 49; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for truck-based refrigerated containers
land::fridge::fridge()
{
	width = 2.05;
	height = 2.10;
	length = 2.21;
	maxLoad = 24588;

	cost = 300;
	valueMod = 1.3;

	type = 2;
	id = std::string("FRG?");
}

// Copy assignment operator for deep copying of ship-based normal containers
land::fridge& land::fridge::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& land::fridge::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	fridge* addedCont(new fridge());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& land::fridge::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	fridge* multipliedCont(new fridge());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on truck-based refrigerated containers
void land::fridge::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Truck" << setCol(WHITE) << "-based " << setCol(TEAL) << "fridge" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for truck-based liquid containers
land::liquid::liquid()
{
	width = 2.19;
	height = 2.33;
	length = 5.48;
	maxLoad = 23401;

	cost = 400;
	valueMod = 1.4;

	type = 3;
	id = std::string("LQD?");
}

// Copy assignment operator for deep copying of ship-based normal containers
land::liquid& land::liquid::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& land::liquid::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	liquid* addedCont(new liquid());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& land::liquid::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	liquid* multipliedCont(new liquid());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on truck-based liquid containers
void land::liquid::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Truck" << setCol(WHITE) << "-based " << setCol(TEAL) << "liquid" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for truck-based hazardous containers
land::hazard::hazard()
{
	width = 2.07;
	height = 2.16;
	length = 5.40;
	maxLoad = 11700;

	cost = 800;
	valueMod = 1.8;

	type = 4;
	id = std::string("HZD?");
}

// Copy assignment operator for deep copying of ship-based normal containers
land::hazard& land::hazard::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& land::hazard::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	hazard* addedCont(new hazard());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& land::hazard::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	hazard* multipliedCont(new hazard());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on truck-based hazardous containers
void land::hazard::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Truck" << setCol(WHITE) << "-based " << setCol(TEAL) << "hazard" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


/*
	PLANE CONTAINERS
					*/

// Default constructor for plane-based normal containers
air::normal::normal()
{
	width = 1.40;
	height = 1.37;
	length = 3.54;
	maxLoad = 13036;

	cost = 100;
	valueMod = 1.1;

	type = 0;
	id = std::string("NRM?");
}

// Copy assignment operator for deep copying of ship-based normal containers
air::normal& air::normal::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& air::normal::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	normal* addedCont(new normal());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& air::normal::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	normal* multipliedCont(new normal());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on plane-based normal containers
void air::normal::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Plane" << setCol(WHITE) << "-based " << setCol(TEAL) << "normal" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for plane-based flat-bed containers
air::flat::flat()
{
	width = 1.46;
	height = 3.00;
	length = 3.38;
	maxLoad = 25260;

	cost = 200;
	valueMod = 1.2;

	type = 1;
	id = std::string("FLT?");
}

// Copy assignment operator for deep copying of ship-based normal containers
air::flat& air::flat::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& air::flat::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	flat* addedCont(new flat());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& air::flat::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	flat* multipliedCont(new flat());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on plane-based flat-bed containers
void air::flat::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 49; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Plane" << setCol(WHITE) << "-based " << setCol(TEAL) << "flat-bed" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                     " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                        " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                     " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                      " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                      " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 49; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for plane-based refrigerated containers
air::fridge::fridge()
{
	width = 1.37;
	height = 1.40;
	length = 1.47;
	maxLoad = 16392;

	cost = 300;
	valueMod = 1.3;

	type = 2;
	id = std::string("FRG?");
}

// Copy assignment operator for deep copying of ship-based normal containers
air::fridge& air::fridge::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& air::fridge::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	fridge* addedCont(new fridge());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& air::fridge::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	fridge* multipliedCont(new fridge());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on plane-based refrigerated containers
void air::fridge::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Plane" << setCol(WHITE) << "-based " << setCol(TEAL) << "fridge" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for plane-based liquid containers
air::liquid::liquid()
{
	width = 1.46;
	height = 1.55;
	length = 3.65;
	maxLoad = 15600;

	cost = 400;
	valueMod = 1.4;

	type = 3;
	id = std::string("LQD?");
}

// Copy assignment operator for deep copying of ship-based normal containers
air::liquid& air::liquid::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& air::liquid::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	liquid* addedCont(new liquid());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& air::liquid::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	liquid* multipliedCont(new liquid());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on plane-based liquid containers
void air::liquid::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Plane" << setCol(WHITE) << "-based " << setCol(TEAL) << "liquid" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{ 0 }; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}


// Default constructor for plane-based hazardous containers
air::hazard::hazard()
{
	width = 1.38;
	height = 1.44;
	length = 3.60;
	maxLoad = 7800;

	cost = 800;
	valueMod = 1.8;

	type = 4;
	id = std::string("HZD?");
}

// Copy assignment operator for deep copying of ship-based normal containers
air::hazard& air::hazard::operator=(container& cont)
{
	// Check for self-assignment of the container
	if (&cont == this) { return *this; }

	// Copy the properties of the reference container into the current container
	valueMod = cont.getValueMod();
	id = cont.getID();

	return *this; // Return the modified container
}

// Overloaded addition operator for ship-based normal containers
container& air::hazard::operator+(container& cont) const
{ // This just adds together the attributes of two containers

	// Create the new container, using our custom assignment operator
	hazard* addedCont(new hazard());
	*addedCont = cont;

	// Add together the values and assign them to the new container
	addedCont->width = width + cont.getWidth();
	addedCont->height = height + cont.getHeight();
	addedCont->length = length + cont.getLength();
	addedCont->maxLoad = maxLoad + cont.getMaxLoad();
	addedCont->cost = cost + cont.getCost();
	addedCont->valueMod = valueMod + cont.getValueMod();
	// Ignoring medium, type and id variables

	return *addedCont; // Return the added container
}

// Overloaded multiplication operator for ship-based normal containers
container& air::hazard::operator*(const int& num) const
{ // This just multiplies the attributes of the container by a scalar, num

	// Create the new container, using the default constructor
	hazard* multipliedCont(new hazard());

	// Multiply the va'lues and assign them to the new container
	multipliedCont->width = width * num;
	multipliedCont->height = height * num;
	multipliedCont->length = length * num;
	multipliedCont->maxLoad = maxLoad * num;
	multipliedCont->cost = cost * num;
	multipliedCont->valueMod = valueMod * num;
	// Ignoring medium, type and id variables

	return *multipliedCont; // Return the multiplied container
}

// Modify the ostream to display the information on plane-based hazardous containers
void air::hazard::dispInfo(std::ostream& os) const
{
	// Top of the frame
	os << setCol(RED) << "  " << box::TLd;
	for (int i{0}; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::TRd << std::endl;
	// Main content
	os << "  " << box::Vd << " " << setCol(TEAL) << "Plane" << setCol(WHITE) << "-based " << setCol(TEAL) << "hazard" << setCol(WHITE) << " containers have properties " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Mass: " << setCol(YELLOW) << maxLoad << setCol(DYELLOW) << " kg                  " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Width: " << setCol(YELLOW) << width << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Height: " << setCol(YELLOW) << height << setCol(DYELLOW) << " m                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Length: " << setCol(YELLOW) << length << setCol(DYELLOW) << " m                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Volume: " << setCol(YELLOW) << std::setprecision(3) << calcVol() << setCol(DYELLOW) << " m^3                 " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Cost: " << setCol(YELLOW) << box::GBP << " " << cost << "                    " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::Vd << setCol(WHITE) << std::setw(22) << std::right << " Base Value: " << setCol(YELLOW) << box::GBP << " " << calcValue() << "                   " << setCol(RED) << box::Vd << std::endl;
	os << "  " << box::BLd;
	// Bottom of the frame
	for (int i{0}; i < 47; i++)
	{
		os << box::Hd;
	}
	os << box::BRd;
}
