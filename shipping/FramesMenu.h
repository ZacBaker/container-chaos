#ifndef FRAMESMENU_H
#define FRAMESMENU_H
// ^ Header guard

// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include "Frames.h"


// Derived class for displaying the title screen/main menu
class title : public frame
{
public:
	title() = default; // Default constructor
	~title() = default; // Destructor

	void mainMenu(); // Void function to house the main menu logic
	void dispFrame() override; // Overriden function to display the title screen/main menu
	static void dispFarewell(); // Function to display the goodbye screen
};

// Derived class for displaying the about page
class about : public frame
{
public:
	about() = default; // Default constructor
	~about() = default; // Destructor

	void dispFrame() override; // Overriden function to display the about page
};

// Derived class for displaying the container reference page
class reference : public title
{
private:
	std::pair<int, int> currentSel; // Tracking where the user's selection is on the container reference screen: .first is the mode of transport and .second is the container type (default to `0,0')
	std::pair<int, int> leftSel, rightSel; // Tracking of the selected right and left container indices
	bool currTopRow; // Flag to track if the user is on the top row (true) or the bottom row (false)
	bool mathMode; // Flag to track if we're in math mode or not
	bool addMode; // Flag to see if the user has chosen to multiply a container by an integer
	bool swapMode; // Flag to container lists
	int factorVal; // The number to scalar multiply a container by, as chosen by the user
	
	void loopSelection(const int incrementRow); // Increment the selection along the row, depending upon the flag `currTopRow'

public:
	// Default constructor
	reference() : currentSel{std::make_pair(0, 0)}, leftSel{std::make_pair(0, 0)}, rightSel{std::make_pair(0, 0)}, currTopRow{true}, mathMode{false}, addMode{true}, swapMode{false}, factorVal{10} {} // Start selection, left and right containers on `Ship' and `Normal', also start at the top row, not in math mode and with addition (not multiplication), and the multiplication factor set to 1
	reference(reference& ref); // Copy constructor
	reference(reference&& ref); // Move constructor

	~reference() = default; // Default destructor

	// Member functions:
	reference& operator=(reference&& ref); // Move assignment operator
	
	void dispFrame() override; // Overriden function to display the reference frame

	bool getSwapMode() const // Return the swapMode flag
	{ return swapMode; }
	void resetSwapMode() // Reset the swapMode flag
	{ swapMode = false; }
};

// End of header guard
#endif // FRAMESMENU_H