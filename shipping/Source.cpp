// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include <memory> // For std::unique_ptr<class>

#include "FramesMenu.h"

// Start of the main program
int main()
{
	// Define and initialise the instances of our title
	std::unique_ptr<title> titleMenu(new title());
	
	titleMenu->mainMenu(); // Start the main menu logic, where the entire game will take place until the user chooses to exit the program
	titleMenu->dispFarewell(); // Display the farewell frame

	return 0; // Terminate the program
}
