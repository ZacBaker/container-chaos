#ifndef SUPPORT_H
#define SUPPORT_H
// ^ Header guard

// Cargo-Chaos
// By Zac Baker
// Contact: mailto:zaclloydbaker@gmail.com?subject=Cargo-Chaos

#include <iostream>
#define NOMINMAX // Suppress the min and max definitions in Windef.h
#include <Windows.h> // Needed for the getKeyPress() function


// An enumerate for the colours available for the console output, 0 to 15 for both text and background
// Reference for the text/background colours: https://rosettacode.org/mw/images/b/bf/Terminal_control,colored_text.png
enum colour
{
	BLACK = 0,
	DBLUE,
	DGREEN,
	DTEAL,
	DRED,
	DPINK,
	DYELLOW,
	GRAY,
	DGRAY,
	BLUE,
	GREEN,
	TEAL,
	RED,
	PINK,
	YELLOW,
	WHITE
};

// A struct that will be used to manipulate the colour of the text and background within the console
struct setCol
{
	colour colTxt, colBG; // Text and background enums
	HANDLE consoleHandle; // A handle for the output console

	explicit setCol(const colour colTxt, const colour colBG = BLACK) : colTxt(colTxt), colBG(colBG), consoleHandle(GetStdHandle(STD_OUTPUT_HANDLE)) {} // Explicit constructor to set both the text and background colours. If no second input argument, default to the normal black background
	// Reference for GetStdHandle(): https://msdn.microsoft.com/en-gb/library/windows/desktop/ms683231(v=vs.85).aspx
};

// Overload the output for altering the output colour of the command prompt
std::basic_ostream<char>& operator <<(std::basic_ostream<char>& ostream, const setCol& colStruct);


// Custom namespace for the box drawing characters used in all the dispFrame() functions and custom scenes
namespace box
{ // Reference table: https://en.wikipedia.org/wiki/Code_page_437#Characters
	// Single lines
	const char H = '\xC4'; // Horizontal
	const char V = '\xB3'; // Vertical
	const char TL = '\xDA'; // Top left corner
	const char TR = '\xBF'; // Top right corner
	const char BL = '\xC0'; // Bottom left corner
	const char BR = '\xD9'; // Bottom right corner
	const char T = '\xC2'; // T
	const char Tu = '\xC1'; // Inverse of T
	const char P = '\xC5'; // +
	const char L = '\xB4'; // Connect to the left
	const char R = '\xC3'; // Connect to the right

	// Double lines
	const char Hd = '\xCD'; // Horizontal
	const char Vd = '\xBA'; // Vertical
	const char TLd = '\xC9'; // Top left corner
	const char TRd = '\xBB'; // Top right corner
	const char BLd = '\xC8'; // Bottom left corner
	const char BRd = '\xBC'; // Bottom right corner
	const char Td = '\xCB'; // T
	const char Tud = '\xCA'; // Inverse of T
	const char Pd = '\xCE'; // +
	const char Ld = '\xB9'; // Connect to the left
	const char Rd = '\xCC'; // Connect to the right

	// Shading characters
	const char S = '\xB1'; // Medium cross shading, 50% pixel fill
	const char SL = '\xB2'; // Heavy diagonal shading, bottom left to top right
	const char SR = '\xB0'; // Light diagonal shading, bottom right to top left
	const char SF = '\xDB'; // Full shading, a solid square

	// Pictoral arrows
	const char arrUD = '\x12'; // Up/Down arrow
	const char arrR = '\x1A'; // Right arrow
	const char arrL = '\x1B'; // Left arrow
	const char arrU = '\x18'; // Up arrow
	const char arrD = '\x19'; // Down arrow

	// Pictoral smile
	const char smile = '\x02'; // Filled smile

	// Currency symbol
	const char GBP = '\x9C'; // �
}


INPUT_RECORD getKeyPress(); // Stand-alone system function to capture and return the key inputs from the console


// End of header guard
#endif // SUPPORT_H
